<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>LACAK</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-3.3.1.min.js' ?>"></script>
</head>
<body>
	
<div class="container">
<br>
	<a href="#form" data-toggle="modal" class="btn btn-primary">Tambah Data</a>
<br>
<br>
	<table class="table">
		<thead class="bg-primary">
			
				<th>Nama Infrastruktur</th>
			
				<th>Bencana Penyebab</th>
			
				<th>Deskripsi Kerusakan</th>

				<th>Jenis Infrastruktur</th>

				<th>Tingkat Kerusakan</th>
			
				<th>Alamat Infrastruktur</th>

				<th>Provinsi</th>

				<th>Kabupaten/Kota</th>

				<th>Kecamatan</th>

				<th>Desa/Kelurahan</th>
			
		</thead>
		<tbody id="target">
			
		</tbody>
	</table>

<div class="modal fade" id="form" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<h2>Formulir Laporan</h2>
			</div>
<center><font color="red"><p id="pesan"></p></font></center>
			<table class="table">
				<tr>
					<td>Nama Infrastruktur</td>
					<td><input type="text" name="nama_infrastruktur" placeholder="Nama Infrastruktur" class="form-control"></td>
				</tr>
					<tr>
					<td>Bencana Penyebab</td>
					<td><input type="text" name="bencana_penyebab" placeholder="Bencana Penyebab" class="form-control"></td>
				</tr>
					<tr>
					<td>Deskripsi Kerusakan</td>
					<td><input type="text" name="desc_kerusakan" placeholder="Deskripsi Kerusakan" class="form-control"></td>
				</tr>
				<tr>
					<td>Jenis Infrastruktur</td>
					<td><select name="jenis_infra" class="form-control">
 					 <option value="1" selected>Puskesmas</option>
 					 <option value="2">Jembatan</option>
					</select></td>
				</tr>
				<tr>
					<td>Tingkat Kerusakan</td>
					<td><select name="tingkat_kerusakan" class="form-control">
 					 <option value="1" selected>Ringan</option>
 					 <option value="2">Sedang</option>
 					 <option value="3">Parah</option>
					</select></td>
				</tr>
					<tr>
					<td>Alamat Infrastruktur</td>
					<td><input type="text" name="alamat_infra" placeholder="Alamat Infrastruktur" class="form-control"></td>
				</tr>
				<tr>
					<td>Provinsi</td>
					<td><select name="provinsi" class="form-control">>
 			 <option value="Sumatera Utara" selected>Sumatera Utara</option>
 			 <option value="diy">DI Yogyakarta</option>
 			 <option value="jatim">Jawa Timur</option>
 			 <option value="ntb">Nusa Tenggara Barat</option>
			</select></td>
				</tr>
				<tr>
					<td>Kabupaten/Kota</td>
					<td><select name="kab_kota" class="form-control">>
 			<option value="Kota Medan" selected>Kota Medan</option>
 			 <option value="diy">DI Yogyakarta</option>
 			 <option value="jatim">Jawa Timur</option>
 			 <option value="ntb">Nusa Tenggara Barat</option>
			</select></td>
				</tr>
				<tr>
					<td>Kecamatan</td>
					<td><select name="kecamatan" class="form-control">>
 			<option value="Medan Johor" selected>Medan Johor</option>
 			 <option value="medan maimun">Medan Maimun</option>
 			 <option value="medankota">Medan Kota</option>
 			 <option value="medansunggal">Medan sunggal</option>
			</select></td>
				</tr>
				<tr>
					<td>Desa/Kelurahan</td>
					<td><select name="desa_kel" class="form-control">>
 			 <option value="Kedai Durian" selected>Kedai Durian</option>
 			 <option value="medanmaimun">Medan Maimun</option>
 			 <option value="medankota">Medan Kota</option>
 			 <option value="medansunggal">Medan sunggal</option>
			</select></td>
				</tr>
				<tr>
					<td></td>
					<td>
						<button type="button" data-dismiss="modal" class="btn btn-secondary">Batal</button>
						<button type="button" id="btn-tambah" onclick="tambahdata()" class="btn btn-primary">Tambah</button>

					</td>
				</tr>
			</table>
		</div>
	</div>
</div>

</div>
<script type="text/javascript">

	ambilData();

	function ambilData(){
	$.ajax({
		type:'POST',
		url: '<?php echo base_url()."index.php/alphacrud/ambildata"?>',
		dataType:'json',
		success: function(data){
			var baris='';
			for(var i=0;i<data.length;i++){
				baris += '<tr>'+
								'<td>'+data[i].nama_infrastruktur+'</td>' +
								'<td>'+data[i].bencana_penyebab+'</td>' +
								'<td>'+data[i].desc_kerusakan+'</td>' +
								'<td>'+data[i].jenis_infra+'</td>' +
								'<td>'+data[i].tingkat_kerusakan+'</td>' +
								'<td>'+data[i].alamat_infra+'</td>' +
								'<td>'+data[i].provinsi+'</td>' +
								'<td>'+data[i].kab_kota+'</td>' +
								'<td>'+data[i].kecamatan+'</td>' +
								'<td>'+data[i].desa_kel+'</td>' +
						'</tr>'
			}
			$('#target').html(baris);
		}
	})
}

	function tambahdata(){
		var nama_infrastruktur=$("[name='nama_infrastruktur']").val();
		var bencana_penyebab=$("[name='bencana_penyebab']").val();
		var desc_kerusakan=$("[name='desc_kerusakan']").val();
		var jenis_infra=$("[name='jenis_infra']").val();
		var tingkat_kerusakan=$("[name='tingkat_kerusakan']").val();
		var alamat_infra=$("[name='alamat_infra']").val();
		var provinsi=$("[name='provinsi']").val();
		var kab_kota=$("[name='kab_kota']").val();
		var kecamatan=$("[name='kecamatan']").val();
		var desa_kel=$("[name='desa_kel']").val();

		$.ajax({
			type:'POST',
			data: 'nama_infrastruktur='+nama_infrastruktur+'&bencana_penyebab='+bencana_penyebab+'&desc_kerusakan='+desc_kerusakan+'&jenis_infra='+jenis_infra+'&tingkat_kerusakan='+tingkat_kerusakan+'&alamat_infra='+alamat_infra+'&provinsi='+provinsi+'&kab_kota='+kab_kota+'&kecamatan='+kecamatan+'&desa_kel='+desa_kel,
			url: '<?php echo base_url().'index.php/alphacrud/tambahdata' ?>',
			dataType: 'json',
			success: function(hasil){
				$("#pesan").html(hasil.pesan);

				if(hasil.pesan==''){
					$("#form").modal('hide');
					ambilData();

					$("[name='nama_infrastruktur']").val('');
					$("[name='bencana_penyebab']").val('');
					$("[name='desc_kerusakan']").val('');
					$("[name='jenis_infra']").val('');
					$("[name='tingkat_kerusakan']").val('');
					$("[name='alamat_infra']").val('');
					$("[name='provinsi']").val('');
					$("[name='kab_kota']").val('');
					$("[name='kecamatan']").val('');
					$("[name='desa_kel']").val('');
				}
			}

		})

	}

</script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Laporanku</title>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>


    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/m-lihatlaporan.css">  
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <h6>Pelapor</h6>

      <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item">
            <a class="navbar-brand" id="lacak" href="<?php echo base_url().'index.php/pelapor/get_laporanku';?>">LACAK</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/pelapor/get_laporanku';?>">Laporanku<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/pelapor/mbuatlaporan';?>">Buat Laporan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="">Pengaturan Akun</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="">Tentang</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/alphacrud/logout';?>">Keluar</a>
          </li>
        </ul>
      </div>
    </nav>

    <div class="container">
      <ul class="nav nav-pills nav-fill">
        <li class="nav-item">
          <a class="nav-link active" id="laporan" href="<?php echo base_url().'index.php/pelapor/get_laporanku';?>">Laporanku</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="buat" href="<?php echo base_url().'index.php/pelapor/mbuatlaporan';?>">Buat Laporan</a>
        </li>
      </ul>
      <div>
        <h5><a class="kembali" href="<?php echo base_url().'index.php/pelapor/get_laporanku';?>">&larr; kembali</a></h5>
        <?php 
    $nomor_laporan=$dataEdit->nomor_laporan;
    $nama_infrastruktur=$dataEdit->nama_infrastruktur;
    $bencana_penyebab=$dataEdit->bencana_penyebab;
    $jenis_infra=$dataEdit->jenis_infra;
    $status=$dataEdit->status;
    $desc_kerusakan=$dataEdit->desc_kerusakan;
    $alamat_infra=$dataEdit->alamat_infra;
    $provinsi=$dataEdit->provinsi;
    $kab_kota=$dataEdit->kab_kota;
    $kecamatan=$dataEdit->kecamatan;
    $desa_kel=$dataEdit->desa_kel;
    $tingkat_kerusakan=$dataEdit->tingkat_kerusakan;
    $url=$dataEdit->url;
    $created_date=$dataEdit->created_date;


  ?><center>
  <h3><?php echo $nama_infrastruktur?></h3>
        <?php 
    if(file_exists($url)){
      ?> 
      <img src="<?php echo base_url($url)?>" alt="" class="featurImg" width="300px"><?php 
    }
    ?>
    <br><br>
        </center>
        <table>
          <tr>
            <td>Tingkat Kerusakan</td>
            <td>:</td>
            <td>
              <?php if($tingkat_kerusakan=='1'){echo'Ringan';}
              elseif ($tingkat_kerusakan=='2') {
               echo 'Sedang';
              } elseif ($tingkat_kerusakan=='3') {
               echo 'Parah';
              } ?>
            </td>
          </tr>
          <tr>
            <td>Bencana Penyebab</td>
            <td>:</td>
            <td><?php echo $bencana_penyebab?></td>
          </tr>
          <tr>
            <td>Alamat</td>
            <td>:</td>
             <td><?php echo $alamat_infra?></td>
          </tr>
          <tr>
            <td>Deskripsi</td>
            <td>:</td>
             <td><?php echo $desc_kerusakan?></td>
          </tr>
          <tr>
            <td>Tanggal Laporan</td>
            <td>:</td>
             <td><?php echo $created_date?></td>
          </tr>
          <tr>
            <td>Status</td>
            <td>:</td>
            <td>
               <?php if($status=='1'){echo'Dikirim ke BPBD';}
              elseif ($status=='2') {
               echo 'Laporan Ditolak';
              } elseif ($status=='3') {
               echo 'Diproses oleh Dinas';
              }elseif ($status=='4') {
               echo 'Selesai Diperbaiki';
              } ?>
            </td>
          </tr>
        </table>
      </div>
    </div>

    
    <footer class="fixed-bottom">&copy 2018 lacak.co.id All Rights Reserved</footer>

    <!-- <div class="chatbox chatbox--tray chatbox--empty">
        <div class="chatbox__title">
            <h5><a href="#">Dinas</a></h5>
            <button class="chatbox__title__tray">
                <span></span>
            </button>
            <button class="chatbox__title__close">
                <span>
                    <svg viewBox="0 0 12 12" width="12px" height="12px">
                        <line stroke="#FFFFFF" x1="11.75" y1="0.25" x2="0.25" y2="11.75"></line>
                        <line stroke="#FFFFFF" x1="11.75" y1="11.75" x2="0.25" y2="0.25"></line>
                    </svg>
                </span>
            </button>
        </div>
        <div class="chatbox__body">
            <div class="chatbox__body__message chatbox__body__message--left">
                <img src="https://s3.amazonaws.com/uifaces/faces/twitter/brad_frost/128.jpg" alt="Picture">
                <p>Curabitur consequat nisl suscipit odio porta, ornare blandit ante maximus.</p>
            </div>
            <div class="chatbox__body__message chatbox__body__message--right">
                <img src="https://s3.amazonaws.com/uifaces/faces/twitter/arashmil/128.jpg" alt="Picture">
                <p>Cras dui massa, placerat vel sapien sed, fringilla molestie justo.</p>
            </div>
            <div class="chatbox__body__message chatbox__body__message--right">
                <img src="https://s3.amazonaws.com/uifaces/faces/twitter/arashmil/128.jpg" alt="Picture">
                <p>Praesent a gravida urna. Mauris eleifend, tellus ac fringilla imperdiet, odio dolor sodales libero, vel mattis elit mauris id erat. Phasellus leo nisi, convallis in euismod at, consectetur commodo urna.</p>
            </div>
        </div>
        <form class="chatbox__credentials">
            <div class="form-group">
                <label for="inputName">Name:</label>
                <input type="text" class="form-control" id="inputName" required>
            </div>
            <div class="form-group">
                <label for="inputEmail">Email:</label>
                <input type="email" class="form-control" id="inputEmail" required>
            </div>
            <button type="submit" class="btn btn-success btn-block">Enter Chat</button>
        </form>
        <textarea class="chatbox__message" placeholder="Write something interesting"></textarea>
    </div> -->



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="../js/popup.js"></script>
  </body>
</html>
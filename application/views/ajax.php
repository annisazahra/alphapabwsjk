<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
     <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/laporanku.css">
    <script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-3.3.1.min.js' ?>"></script>


    <title>Laporanku</title>
  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a id="judul" class="navbar-brand" href="home_login.php">LACAK</a>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a id="menu" class="nav-item nav-link" href="<?php echo base_url().'index.php/alphacrud/buatlaporan' ?>">Buat Laporan<span class="sr-only">(current)</span></a>
      <a id="menu" class="nav-item nav-link" href="laporanku.php">Laporanku</a>
      <a id="menu" class="nav-item nav-link" href="pengaturan.php">Pengaturan Akun</a>
      <a id="menu" class="nav-item nav-link" href="about_login.php">Tentang</a>
      <a id="menu" class="nav-item nav-link" href="<?php echo base_url().'index.php/alphacrud/logout' ?>">Keluar</a>
    </div>
  </div>
  <p><?php echo $this->session->userdata('username'); ?></p>
</nav>

<div class="konten">
  <div class="bar">
    <ul class="nav nav-pills nav-fill" id="pills-tab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="pills-home-tab" aria-controls="pills-home" aria-selected="true" href="laporanku.php">Laporanku</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-profile-tab" aria-controls="pills-profile" aria-selected="true" href="<?php echo base_url().'index.php/alphacrud/buatlaporan' ?>">Buat Laporan</a>
    </li>
  </ul>
  </div>
</div>

<div class="container">
  <h1>Laporanku</h1>
  <table class="table">
    <thead>
      <tr>
        <th>Nomor Laporan</th>
        <th>Nama Infra</th>
        <th>Bencana</th>
        <th>Jenis</th>
        <th>Tingkat</th>
        <th>Alamat</th>
        <th>Provinsi</th>
        <th>Kab</th>
        <th>Kecamatan</th>
        <th>Desa</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody id="target">
      


    </tbody>
  </table>
</div>
<div class="footer">
  <p>Copyright &copy Alpha 2018.</p>
</div>

<script type="text/javascript">
  function ambilData(){
  $.ajax({
  type:'POST',
  url: '<?php echo base_url()."index.php/alphacrud"?>',
  dataType: 'json',
  success:function(data){
    
  }

})
}

</script>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
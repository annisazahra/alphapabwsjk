<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/masyarakat.css">

    <title>Sign Up Masyarakat</title>
  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a id="judul" class="navbar-brand" href="home.php">LACAK</a>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a id="menu" class="nav-item nav-link" href="masyarakat.php">Daftar <span class="sr-only">(current)</span></a>
      <a id="menu" class="nav-item nav-link" href="login.php">Masuk</a>
      <a id="menu" class="nav-item nav-link" href="about.php">Tentang</a>
      <a id="menu" class="nav-item nav-link" href="kontak.php">Kontak</a>
    </div>
  </div>
  <a id="daftar" class="btn btn-default" href="masyarakat.php">Daftar</a>
</nav>

<div class="konten">
  <a id="kembali" href="daftar.php">< kembali</a>
  <h1 class="judul">LACAK</h1>
  <div class="row">
    <div class="col-sm-6 col-xs-12">
      <li>
        <a id="facebook" class="btn btn-default" href="">Login dengan Facebook</a>  
      </li>
      <li>
        <a id="twitter" class="btn btn-default" href="">Login dengan Twitter</a>  
      </li>
      <li>
        <a id="google" class="btn btn-default" href="">Login dengan Google</a>  
      </li>
    </div>
    
    <div class="clearfix visible-sm"></div>
     <div id="or" class="col">
        <p>atau dengan email</p>
    </div>
    
    <div class="col-sm-4 col-xs-10">
      <form id="login" action="home_login.php" method="post">
        <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" class="form-control" id="nama" placeholder="Nama Lengkap">
        </div>
        <div class="form-group">
          <label for="alamat">Alamat</label>
          <textarea class="alamat" rows="4" cols="42" placeholder="Alamat"></textarea>
        </div>
        <div class="form-group">
          <label for="provinsi">Provinsi</label>
            <select id="provinsi" class="form-control">
              <option value="">Provinsi</option>
              <option>...</option>
              <option>...</option>
            </select>
        </div>
        <div class="form-group">
          <label for="kabupaten">Kabupaten</label>
            <select id="kabupaten" class="form-control">
              <option value="">Kabupaten</option>
              <option>...</option>
              <option>...</option>
            </select>
        </div>
        <div class="form-group">
          <label for="kecamatan">Kecamatan</label>
            <select id="kecamatan" class="form-control">
              <option value="">Kecamatan</option>
              <option>...</option>
              <option>...</option>
            </select>
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Masukkan email">
        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <input type="password" class="form-control" id="password" placeholder="Password">
        </div>
        <button type="submit" id="tombol" class="btn btn-primary">Daftar</button>
      </form>
    </div>
  </div>
</div>

<div class="footer">
  <p>Copyright &copy Alpha 2018.</p>
</div>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
<!DOCTYPE html>  
 <head>  
   <title><?php echo $title; ?></title>  
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
      <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
      <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />  
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  

   <style>  
           body  
           {  
                margin:0;  
                padding:0;  
                background-color:#f1f1f1;  
           }  
           .box  
           {  
                width:900px;  
                padding:20px;  
                background-color:#fff;  
                border:1px solid #ccc;  
                border-radius:5px;  
                margin-top:10px;  
           }  
      </style>  
 </head>  
 <body>  
      <div class="container box">  
           <h3 align="center"><?php echo $title; ?></h3><br />  
           <div class="table-responsive">  
                <br />  
                <table id="user_data" class="table table-bordered table-striped">  
                     <thead>  
                          <tr>  
                              <th width="35%">Nomor Laporan</th> 
                               <th width="35%">Nama Infra</th>  
                               <th width="35%">Bencana Penyebab</th>  
                               <th width="35%">Deskripsi Kerusakan</th>  
                               <th width="35%">Jenis Infrastruktur</th>  
                               <th width="35%">Tingkat Kerusakan</th>  
                               <th width="35%">Alamat</th>  
                               <th width="35%">Provinsi</th>  
                               <th width="35%">Kabupaten/Kota</th>  
                               <th width="35%">Kecamatan</th>  
                               <th width="35%">Desa/Kelurahan</th>  
                               <th width="35%">Status</th>  
                               <th width="10%">Edit</th>  
                               <th width="10%">Delete</th>  
                          
                          </tr>  
                     </thead>  
                </table>  
           </div>  
      </div>  
 </body>  
 </html>  

<div id="userModal" class="modal fade">  
      <div class="modal-dialog">  
           <form method="post" id="user_form">  
                <div class="modal-content">  
                     <div class="modal-header">  
                          <button type="button" class="close" data-dismiss="modal">&times;</button>  
                          <h4 class="modal-title">Add Laporan</h4>  
                     </div>  
                     <div class="modal-body">  
                          <label>Enter Nama Infrastruktur</label>  
                          <input type="text" name="nama_infrastruktur" id="nama_infrastruktur" class="form-control" />  
                          <br />  
                          <label>Enter Bencana Penyebab</label>  
                          <input type="text" name="bencana_penyebab" id="bencana_penyebab" class="form-control" />  
                          <br />  
                          <label>Enter Deskripsi Kerusakan</label>  
                          <input type="text" name="desc_kerusakan" id="desc_kerusakan" class="form-control" />  
                          <br />  
                          <label>Enter Jenis Infrastruktur</label>  
                          <input type="text" name="jenis_infra" id="jenis_infra" class="form-control" />  
                          <br />  
                          <label>Enter Tingkat Kerusakan</label>  
                          <input type="text" name="tingkat_kerusakan" id="tingkat_kerusakan" class="form-control" />  
                          <br />  
                          <label>Enter Alamat</label>  
                          <input type="text" name="alamat_infra" id="alamat_infra" class="form-control" />  
                          <br />  
                          <label>Enter Provinsi</label>  
                          <input type="text" name="provinsi" id="provinsi" class="form-control" />  
                          <br />  
                          <label>Enter Kabupaten/Kota</label>  
                          <input type="text" name="kab_kota" id="kab_kota" class="form-control" />  
                          <br />  
                          <label>Enter Kecamatan</label>  
                          <input type="text" name="kecamatan" id="kecamatan" class="form-control" />  
                          <br />  
                          <label>Enter Desa/Kelurahan</label>  
                          <input type="text" name="desa_kel" id="desa_kel" class="form-control" />  
                          <br />  
                          <label>Enter Status</label>  
                          <input type="text" name="status" id="status" class="form-control" />  
                          <br />  
                          
                     </div>  
                     <div class="modal-footer">  
                          <input type="hidden" name="nomor_laporan" id="nomor_laporan" />  
                          <input type="submit" name="action" id="action" class="btn btn-success" value="Add" />  
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                     </div>  
                </div>  
           </form>  
      </div>  
   
</div>


 <script type="text/javascript" language="javascript" >  
 $(document).ready(function(){  
  $('#add_button').click(function(){  
           $('#user_form')[0].reset();  
           $('.modal-title').text("Add User");  
           $('#action').val("Add");   
      })  

      var dataTable = $('#user_data').DataTable({  
           "processing":true,  
           "serverSide":true,  
           "order":[],  
           "ajax":{  
                url:"<?php echo base_url() . 'index.php/webslessonc/fetch_user'; ?>",  
                type:"POST"  
           },  
           "columnDefs":[  
                {  
                     "targets":[0, 12,13],  
                     "orderable":false,  
                },  
           ],  
      }); 

      $(document).on('submit', '#user_form', function(event){  
           event.preventDefault();  
           var nama_infrastruktur = $('#nama_infrastruktur').val();  
           var bencana_penyebab = $('#bencana_penyebab').val();        
           var desc_kerusakan = $('#desc_kerusakan').val();        
           var jenis_infra = $('#jenis_infra').val();        
           var tingkat_kerusakan = $('#tingkat_kerusakan').val();        
           var alamat_infra = $('#alamat_infra').val();        
           var provinsi = $('#provinsi').val();        
           var kab_kota = $('#kab_kota').val();        
           var kecamatan = $('#kecamatan').val();        
           var desa_kel = $('#desa_kel').val();        
           var status = $('#status').val();        
           if(nama_infrastruktur != '' && bencana_penyebab != '')  
           {  
                $.ajax({  
                     url:"<?php echo base_url() . 'index.php/webslessonc/user_action'?>",  
                     method:'POST',  
                     data:new FormData(this),  
                     contentType:false,  
                     processData:false,  
                     success:function(data)  
                     {  
                          alert(data);  
                          $('#user_form')[0].reset();  
                          $('#userModal').modal('hide');  
                          dataTable.ajax.reload();  
                     }  
                });  
           }  
           else  
           {  
                alert("Bother Fields are Required");  
           }  
      });  
      $(document).on('click', '.update', function(){  
           var nomor_laporan = $(this).attr("id");  
           $.ajax({  
                url:"<?php echo base_url(); ?>index.php/webslessonc/fetch_single_user",  
                method:"POST",  
                data:{nomor_laporan:nomor_laporan},  
                dataType:"json",  
                success:function(data)  
                {  
                     $('#userModal').modal('show');  
                     $('#nama_infrastruktur').val(data.nama_infrastruktur);  
                     $('#bencana_penyebab').val(data.bencana_penyebab);  
                     $('#desc_kerusakan').val(data.desc_kerusakan);  
                     $('#jenis_infra').val(data.jenis_infra);  
                     $('#tingkat_kerusakan').val(data.tingkat_kerusakan);  
                     $('#alamat_infra').val(data.alamat_infra);  
                     $('#provinsi').val(data.provinsi);  
                     $('#kab_kota').val(data.kab_kota);  
                     $('#kecamatan').val(data.kecamatan);  
                     $('#desa_kel').val(data.desa_kel);  
                     $('#status').val(data.status);  
                     $('.modal-title').text("Edit Laporan");  
                     $('#nomor_laporan').val(nomor_laporan);  
                     $('#action').val("Edit");  
                }  
           })  
      });  

 });  
 </script>  

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Laporanku</title>
    <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/m_laporanku.css">  
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <h6>Pelapor</h6>

      <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item">
            <a class="navbar-brand" id="lacak" href="<?php echo base_url().'index.php/pelapor/get_laporanku';?>">LACAK</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/pelapor/get_laporanku';?>">Laporanku<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/pelapor/mbuatlaporan';?>">Buat Laporan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="">Pengaturan Akun</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="">Tentang</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/alphacrud/logout';?>">Keluar</a>
          </li>
        </ul>
      </div>
    </nav>

    <div class="container">
      <ul class="nav nav-pills nav-fill">
        <li class="nav-item">
          <a class="nav-link active" id="laporan" href="<?php echo base_url().'index.php/pelapor/get_laporanku';?>">Laporanku</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="buat" href="<?php echo base_url().'index.php/pelapor/mbuatlaporan';?>">Buat Laporan</a>
        </li>
      </ul>
  <form action="<?php echo base_url('index.php/pelapor/hasil')?>" action="GET">
        <div class="form-group row">
          <div class="col-11">
          
          <br>
        <input type="text" class="form-control" id="cari" name="cari" placeholder="Pencarian"> 
        <input class="btn btn-primary btn-sm" type="submit" value="Cari">
        </div>
      </div>
      </form><br><br>

<form action="<?php echo base_url('index.php/pelapor/urutkan')?>" action="GET">
  <div class="form-group row">
    <div class="col-11">
      <select name="urutkan" id="urutkan" class="form-control">
       <option value="" selected>Urut Berdasarkan</option>
       <option value="nomor_laporan">Nomor Laporan</option>
       <option value="created_date">Tanggal Laporan</option>
       <option value="nama_infrastruktur">Nama Infrastruktur</option>
       <option value="status">Status</option>
      </select>
      <input class="btn btn-primary btn-sm" type="submit" value="Urutkan">

    </div>
    
  </div>
</form>

 <h3>Hasil Pengurutan</h3>
          <table class="table">
            <tr>
              <th>Nomor Laporan</th>
              <th>Nama Infrastruktur</th>
              <th>Status</th>
              <th>Lihat Laporan</th>
            </tr>
<?php
 
    if(count($urutkan)>0)
    {
      foreach ($urutkan as $data) {?>
     <!--  echo $data->nomor_laporan . " => " . $data->nama_infrastruktur ."<br>";
      -->
      <tr>
      <td><?php 
      echo $data->nomor_laporan;
     $nomor_laporan = $data->nomor_laporan ?></td>
      <td><?php echo $data->nama_infrastruktur; ?></td>
      <td>
      <?php 
      $status = $data->status;
      if ($status=="1"){
      echo "Dikirim ke BPBD";
      }elseif ($status=="2"){
      echo "Laporan Ditolak";
      }elseif ($status=="3"){
      echo "Diproses oleh Dinas";
      }elseif ($status=="4"){
      echo "Selesai";
      } ?>
     </td>
      <td> <div class="col-7">
          <a class="lihat" href="<?php echo base_url('index.php/pelapor/edit/'.$data->nomor_laporan)?>">Lihat Laporan</a>
        </div></td>
    
    </tr>
    <?php } ?>
     <?php } 
 
    else
    {
      echo "Data tidak ditemukan";
    }
 
    ?>
</table>
    </div>
  </div>
</div>

 




    </div>
    
    <footer class="fixed-bottom">&copy 2018 lacak.co.id All Rights Reserved</footer>

   
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="../js/popup.js"></script>
  </body>
</html>
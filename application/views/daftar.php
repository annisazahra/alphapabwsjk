<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Daftar</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/signup-daftar.css">  
</head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      
       <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item">
            <a class="navbar-brand" id="lacak" href="<?php echo base_url().'index.php/alphacrud/indexxx';?>">LACAK</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/alphacrud/daftar';?>">Daftar <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/alphacrud/masuk';?>">Masuk</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="">Tentang</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/alphacrud/kontak';?>">Kontak</a>
          </li>
        </ul>
      </div>
    </nav>

    <div class="container">
      <form method="post" action="<?php echo site_url('alphacrud/register');?>">
        <h1>Daftar</h1>
        <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" class="form-control" name="nama"id="nama" placeholder="Nama Lengkap">
        </div>
       
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" class="form-control" id="username" name="username" aria-describedby="emailHelp" placeholder="Masukkan email">
        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <input type="password" class="form-control" name="password" id="password" placeholder="Password">
        </div>
        <center><button type="submit" name="submit" class="btn btn-primary">Daftar</button></center>
      </form>

      <h5 style="text-align: center;">Sudah punya akun?</h5>
      <div class="bottom-container">
        <div class="row">
          <div class="col">
            <a href="<?php echo base_url().'index.php/alphacrud/masuk';?>" class="btn">Masuk</a>
          </div>
        </div>
      </div>
    </div>    

    <footer class="fixed-bottom">&copy 2018 lacak.co.id All Rights Reserved</footer>




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
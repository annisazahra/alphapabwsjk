<!doctype html>
<html lang="en">
<head>
	 <meta charset="utf-8">
	 <link rel="stylesheet" type="text/css" href="">
</head>
<body>

	<form>
		<table>
		 <tr>
      <td>Jenis Infrastruktur</td>
      
      <td><select name="jenis_infra">
       <option value="1" selected>Puskesmas</option>
       <option value="2">Jembatan</option>
       <option value="3">Rumah Sakit</option>
       <option value="4">Jalan Raya</option>
       <option value="5">Jalan Tol</option>
       <option value="6">Jalur Rel Kereta Api</option>
       <option value="7">Lembaga Pemasyarakatan</option>
       <option value="8">Perkantoran</option>
       <option value="9">Jalan</option>
       <option value="10">Terowongan</option>
       <option value="11">Lampu Jalan</option>
       <option value="12">Rambu Lalu Lintas</option>
       <option value="13">Pelabuhan</option>
       <option value="14">Bandar Udara</option>
       <option value="15">Taman</option>
       <option value="16">Sekolah Negeri</option>
       <option value="17">Universitas Negeri</option>
       <option value="18">Stadion</option>


      </select></td>
    </tr>
    </table>
	</form>






	 <div id="chart_div"></div>

	 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<!--  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
	<canvas id="popChart" width="800" height="200" style="margin-top: 50px;">
<script>
var ctx = document.getElementById("popChart").getContext('2d');

var data_nama = [];
var data_jumlah = [];

$.post("<?php  echo base_url('index.php/alphacrud/getData') ?> ",
function (data){
	var obj = JSON.parse(data);
	$.each(obj, function(test,item){
		data_nama.push(item.jenisInfra);
		data_jumlah.push(item.jumlah);
	});


var lineChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: data_nama,
        datasets: [{
            label: 'Jumlah Laporan',
            data: data_jumlah,
            backgroundColor: 
                'rgba(17, 28, 238, 0.55)',
             
            borderColor: 
                'rgba(17, 28, 238, 0.55)',
               
            pointBorderColor:"rgba(17, 28, 238, 0.6)",
            pointBackgroundColor:"rgba(38,185,154,0.7)",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor:"rgba(220,220,220,1)",   
         
            pointBorderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
	});
});
</script>

	
</body>
</html>
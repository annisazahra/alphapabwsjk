<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/buat_laporan.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>


    <title>Home</title>
  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a id="judul" class="navbar-brand" href="home_login.php">LACAK</a>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a id="menu" class="nav-item nav-link" href="buat_laporan.php">Buat Laporan<span class="sr-only">(current)</span></a>
      <a id="menu" class="nav-item nav-link" href="<?php echo base_url().'index.php/pelapor/get_laporanku' ?>">Laporanku</a>
      <a id="menu" class="nav-item nav-link" href="pengaturan.php">Pengaturan Akun</a>
      <a id="menu" class="nav-item nav-link" href="about_login.php">Tentang</a>
      <a id="menu" class="nav-item nav-link" href="Home.php">Keluar</a>
    </div>
  </div>
  <p><?php echo $this->session->userdata('username'); ?></p>
</nav>


<div class="konten">
    <div class="bar">
      <ul class="nav nav-pills nav-fill" id="pills-tab" role="tablist">
        <li class="nav-item">
          <a class="nav-link" id="pills-home-tab" aria-controls="pills-home" aria-selected="true" href="<?php echo base_url().'index.php/pelapor/get_laporanku' ?>">Laporanku</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" id="pills-profile-tab" aria-controls="pills-profile" aria-selected="true" href="buat_laporan.php">Buat Laporan</a>
        </li>
      </ul>
    </div>
  <form method="POST" action="<?php echo base_url()."index.php/pelapor/insert"; ?>" enctype="multipart/form-data">
   <!--  <?php 
    $nomor_laporan=$dataEdit->nomor_laporan;
    ?> -->
    <h3>Laporan Kerusakan</h3>
    <div class="form-group">
      <table type="table">
    <tr>
      <td>Nama Infrastruktur</td>
      
      <td><input type="text" name="nama_infrastruktur"/></td>
    </tr>
    <tr>
      <td>Bencana Penyebab</td>
      
      <td><input type="text" name="bencana_penyebab"/></td>
    </tr>
      <tr>
      <td>Deskripsi Kerusakan</td>
      
      <td><input type="text" name="desc_kerusakan"/></td>
    </tr>
    <tr>
      <td>Jenis Infrastruktur</td>
      
      <td><select name="jenis_infra">
       <option value="1" selected>Puskesmas</option>
       <option value="2">Jembatan</option>
       <option value="3">Rumah Sakit</option>
       <option value="4">Jalan Raya</option>
       <option value="5">Jalan Tol</option>
       <option value="6">Jalur Rel Kereta Api</option>
       <option value="7">Lembaga Pemasyarakatan</option>
       <option value="8">Perkantoran</option>
       <option value="9">Jalan</option>
       <option value="10">Terowongan</option>
       <option value="11">Lampu Jalan</option>
       <option value="12">Rambu Lalu Lintas</option>
       <option value="13">Pelabuhan</option>
       <option value="14">Bandar Udara</option>
       <option value="15">Taman</option>
       <option value="16">Sekolah Negeri</option>
       <option value="17">Universitas Negeri</option>
       <option value="18">Stadion</option>


      </select></td>
    </tr>
    <tr>
      <td>Tingkat Kerusakan</td>
      <td><input type="radio" name="tingkat_kerusakan" value="1"> Ringan</td>
    </tr>
    <tr>
      <td></td>
      <td><input type="radio" name="tingkat_kerusakan" value="2"> Sedang</td>
    </tr>
    <tr>
      <td></td>
      <td><input type="radio" name="tingkat_kerusakan" value="3"> Parah</td>
    </tr>
    <tr>
      <td>Alamat Infrastruktur</td>
      
      <td><input type="text" name="alamat_infra" class="form-control input-lg"/></td>
    </tr>
      <tr>
      <td>Provinsi</td>
      
      <td><select name="provinsi" id="provinsi" class="form-control input-lg">
      <option value="">Select Provinsi</option>
        <?php
        foreach($provinsi as $row)
        {
        echo '<option value="'.$row->provinsi_id.'">'.$row->provinsi_name.'</option>';
        }
        ?>
      </select></td>
    </tr>
    <tr>
      <td>Kabupaten</td>
      
      <td><select name="kab_kota" id="kab_kota" class="form-control input-lg">
       <option value="">Select Kabupaten/Kota</option>
      </select></td>
    </tr>
      <tr>
      <td>Kecamatan</td>
      
      <td><select name="kecamatan" id="kecamatan" class="form-control input-lg">
        <option value="">Select Kecamatan</option>
      </select></td>
    </tr>
    </tr>
      <tr>
      <td>Desa/Kelurahan</td>
      
      <td><select name="desa_kel" id="desa_kel" class="form-control input-lg">
       <option value="">Select Desa/Kelurahan</option>
      </select></td>
    </tr>
    <tr>
    <td>Bukti Kerusakan</td>
      <td><input type="file" name="pics" id="pics" class="form-control input-lg"></td>
    </tr>
    <tr>
      <?php echo $this->session->userdata('id'); ?>
      <td><input type="submit" value="Kirim"></td>
      
    </tr>
  

  </table>
</div>
</form>


<div class="footer">
  <p>Copyright &copy Alpha 2018.</p>
</div>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!--     <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
 -->    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>

<script>
$(document).ready(function(){
 $('#provinsi').change(function(){
  var provinsi_id = $('#provinsi').val();
  if(provinsi_id != '')
  {
   $.ajax({
    url:"<?php echo base_url(); ?>index.php/alphacrud/fetch_kab_kota",
    method:"POST",
    data:{provinsi_id:provinsi_id},
    success:function(data)
    {
     $('#kab_kota').html(data);
     $('#kecamatan').html('<option value="">Select Kecamatan</option>');
     $('#desa_kel').html('<option value="">Select Desa/Kelurahan</option>');
    }
   });
  }
  else
  {
   $('#kab_kota').html('<option value="">Select Kabupaten/Kota</option>');
   $('#kecamatan').html('<option value="">Select Kecamatan</option>');
   $('#desa_kel').html('<option value="">Select Desa/Kelurahan</option>');

  }
 });

 $('#kab_kota').change(function(){
  var kab_kota_id = $('#kab_kota').val();
  if(kab_kota_id != '')
  {
   $.ajax({
    url:"<?php echo base_url(); ?>index.php/alphacrud/fetch_kecamatan",
    method:"POST",
    data:{kab_kota_id:kab_kota_id},
    success:function(data)
    {
     $('#kecamatan').html(data);
     $('#desa_kel').html('<option value="">Select Desa/Kelurahan</option>');

    }
   });
  }
  else
  {
   $('#kecamatan').html('<option value="">Select Kecamatan</option>');
   $('#desa_kel').html('<option value="">Select Desa/Kelurahan</option>');

  }
 });

 $('#kecamatan').change(function(){
  var kecamatan_id = $('#kecamatan').val();
  if(kecamatan_id != '')
  {
   $.ajax({
    url:"<?php echo base_url(); ?>index.php/alphacrud/fetch_desa_kel",
    method:"POST",
    data:{kecamatan_id:kecamatan_id},
    success:function(data)
    {
     $('#desa_kel').html(data);
    }
   });
  }
  else
  {
   $('#desa_kel').html('<option value="">Select Desa/Kelurahan</option>');
  }
 });
 
});
</script>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href=../css/icon/open-iconic-bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/pengaturan.css">

    <title>Pengaturan Akun</title>
  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a id="judul" class="navbar-brand" href="home_login.php">LACAK</a>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a id="menu" class="nav-item nav-link" href="pengaturan_akun.php">Buat Laporan<span class="sr-only">(current)</span></a>
      <a id="menu" class="nav-item nav-link" href="pengaturan_akun.php">Laporanku</a>
      <a id="menu" class="nav-item nav-link" href="pengaturan.php">Pengaturan Akun</a>
      <a id="menu" class="nav-item nav-link" href="about_login.php">Tentang</a>
      <a id="menu" class="nav-item nav-link" href="Home.php">Keluar</a>
    </div>
  </div>
  <p>NamaAkun</p>
</nav>

<div class="konten">
  <h4 class="setting">Pengaturan Akun</h4>
  <img class="profil" src="../img/download.png">
  <div class="biodata">
    <li>
      <img class="icon" src="../icon/baseline_account_circle_black_18dp.png">
      <p class="data">Nama</p>
    </li>
    <li>
      <img class="icon" src="../icon/baseline_place_black_18dp.png">
      <p class="data">Alamat</p>
    </li>
    <li>
      <img class="icon" src="../icon/baseline_email_black_18dp.png">
      <p class="data">email</p>
    <li>
      <img class="icon" src="../icon/baseline_call_black_18dp.png">
      <p class="data">no telp</p>
    </li>
  </div>  
  <a id="tombol" class="btn btn-default" href="edit_profil.php">Edit Profil</a>
</div>






<div class="footer">
  <p>Copyright &copy Alpha 2018.</p>
</div>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
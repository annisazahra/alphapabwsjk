<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
     <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/laporanku.css">
    <script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-3.3.1.min.js' ?>"></script>


    <title>Laporanku</title>
  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a id="judul" class="navbar-brand" href="home_login.php">LACAK</a>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a id="menu" class="nav-item nav-link" href="<?php echo base_url().'index.php/pelapor/buatlaporan' ?>">Buat Laporan<span class="sr-only">(current)</span></a>
      <a id="menu" class="nav-item nav-link" href="<?php echo base_url().'index.php/pelapor/get_laporanku' ?>">Laporanku</a>
      <a id="menu" class="nav-item nav-link" href="pengaturan.php">Pengaturan Akun</a>
      <a id="menu" class="nav-item nav-link" href="about_login.php">Tentang</a>
      <a id="menu" class="nav-item nav-link" href="<?php echo base_url().'index.php/alphacrud/logout' ?>">Keluar</a>
    </div>
  </div>
  <p><?php echo $this->session->userdata('username'); ?></p>
</nav>

<div class="konten">
  <div class="bar">
    <ul class="nav nav-pills nav-fill" id="pills-tab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="pills-home-tab" aria-controls="pills-home" aria-selected="true" href="<?php echo base_url().'index.php/pelapor/get_laporanku' ?>">Laporanku</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-profile-tab" aria-controls="pills-profile" aria-selected="true" href="<?php echo base_url().'index.php/pelapor/buatlaporan' ?>">Buat Laporan</a>
    </li>
  </ul>
  </div>
</div>
	<?php 
		$nomor_laporan=$dataEdit->nomor_laporan;
		$nama_infrastruktur=$dataEdit->nama_infrastruktur;
		$bencana_penyebab=$dataEdit->bencana_penyebab;
		$jenis_infra=$dataEdit->jenis_infra;
		$status=$dataEdit->status;
		$desc_kerusakan=$dataEdit->desc_kerusakan;
		$alamat_infra=$dataEdit->alamat_infra;
		$provinsi=$dataEdit->provinsi;
		$kab_kota=$dataEdit->kab_kota;
		$kecamatan=$dataEdit->kecamatan;
		$desa_kel=$dataEdit->desa_kel;
		$tingkat_kerusakan=$dataEdit->tingkat_kerusakan;
		$url=$dataEdit->url;


	?>

	<form>
		<?php 
		if(file_exists($url)){
			?> 
			<img src="<?php echo base_url($url)?>" alt="" class="featurImg" width="300px"><?php 
		}
		?><br><br>
		<label class="control-label col-sm-2">Nomor Laporan</label>
		<input type="text" name="nomor_laporan" value="<?php echo $nomor_laporan?>" readonly><br>
		<label class="control-label col-sm-2">Nama Infrastruktur</label>
		<input type="text" name="nama_infrastruktur" value="<?php echo $nama_infrastruktur?>" readonly><br>
		<label class="control-label col-sm-2">Bencana Penyebab</label>
		<input type="text" name="bencana_penyebab" value="<?php echo $bencana_penyebab?>" readonly><br>
		 <label class="control-label col-sm-2">Tingkat Kerusakan </label>
    <select name="tingkat_kerusakan" id="tingkat_kerusakan">
    <option name="tingkat_kerusakan" <?php if($tingkat_kerusakan=='1'){echo'selected';} ?> value="1">Ringan</option>
    <option name="tingkat_kerusakan" <?php if($tingkat_kerusakan=='2'){echo'selected';} ?> value="2">Sedang</option>
    <option name="tingkat_kerusakan" <?php if($tingkat_kerusakan=='3'){echo'selected';} ?> value="3">Parah</option>
    </select><br>
		<label class="control-label col-sm-2">Jenis Infrastruktur </label>
		<select id="jenis_infra" name="jenis_infra">
    <option name="jenis_infra" <?php if($jenis_infra=='1'){echo'selected';} ?> value="1">Puskesmas</option>
    <option name="jenis_infra" <?php if($jenis_infra=='2'){echo'selected';} ?> value="2">Jembatan</option>
    <option name="jenis_infra" <?php if($jenis_infra=='3'){echo'selected';} ?> value="3">Rumah Sakit</option>
    <option name="jenis_infra" <?php if($jenis_infra=='4'){echo'selected';} ?> value="4">Jalan Raya</option>
    <option name="jenis_infra" <?php if($jenis_infra=='5'){echo'selected';} ?> value="5">Jalan Tol</option>
    <option name="jenis_infra" <?php if($jenis_infra=='6'){echo'selected';} ?> value="6">Jalur Rel Kereta Api</option>
    <option name="jenis_infra" <?php if($jenis_infra=='7'){echo'selected';} ?> value="7">Lembaga Pemasyarakatan</option>
    <option name="jenis_infra" <?php if($jenis_infra=='8'){echo'selected';} ?> value="8">Perkantoran</option>
    <option name="jenis_infra" <?php if($jenis_infra=='9'){echo'selected';} ?> value="9">Jalan</option>
    <option name="jenis_infra" <?php if($jenis_infra=='10'){echo'selected';} ?> value="10">Terowongan</option>
    <option name="jenis_infra" <?php if($jenis_infra=='11'){echo'selected';} ?> value="11">Lampu Jalan</option>
    <option name="jenis_infra" <?php if($jenis_infra=='12'){echo'selected';} ?> value="12">Rambu Lalu Lintas</option>
    <option name="jenis_infra" <?php if($jenis_infra=='13'){echo'selected';} ?> value="13">Pelabuhan</option>
    <option name="jenis_infra" <?php if($jenis_infra=='14'){echo'selected';} ?> value="14">Bandar Udara</option>
    <option name="jenis_infra" <?php if($jenis_infra=='15'){echo'selected';} ?> value="15">Taman</option>
    <option name="jenis_infra" <?php if($jenis_infra=='16'){echo'selected';} ?> value="16">Sekolah Negeri</option>
    <option name="jenis_infra" <?php if($jenis_infra=='17'){echo'selected';} ?> value="17">Universitas Negeri</option>
    <option name="jenis_infra" <?php if($jenis_infra=='18'){echo'selected';} ?> value="18">Stadion</option>
    </select><br>
  		<label class="control-label col-sm-2">Deskripsi Kerusakan </label>
		<input type="text" name="desc_kerusakan" value="<?php echo $desc_kerusakan?>" readonly><br>
		<label class="control-label col-sm-2">Alamat Infrastruktur </label>
		<input type="text" name="alamat_infra" value="<?php echo $alamat_infra?>" readonly><br>
		<label class="control-label col-sm-2">Provinsi </label>
		<select id="provinsi" name="provinsi">
    <option name="provinsi" <?php if($provinsi=='1'){echo'selected';} ?> value="1">Aceh</option>
    <option name="provinsi" <?php if($provinsi=='2'){echo'selected';} ?> value="2">Sumatera Utara</option>
    <option name="provinsi" <?php if($provinsi=='3'){echo'selected';} ?> value="3">DI Yogyakarta</option>
    <option name="provinsi" <?php if($provinsi=='4'){echo'selected';} ?> value="4">NTT</option>
    </select><br>
		<label class="control-label col-sm-2">Kabupaten/Kota </label> 
		<select id="kab_kota" name="kab_kota"> 
	<option name="kab_kota" <?php if($kab_kota=='1'){echo'selected';} ?> value="1">Kabupaten Aceh Barat</option>
    <option name="kab_kota" <?php if($kab_kota=='2'){echo'selected';} ?> value="2">Kabupaten Gayo Lues</option>
    <option name="kab_kota" <?php if($kab_kota=='3'){echo'selected';} ?> value="3">Kota Banda Aceh</option>
    <option name="kab_kota" <?php if($kab_kota=='4'){echo'selected';} ?> value="4">Kota Medan</option>
    <option name="kab_kota" <?php if($kab_kota=='5'){echo'selected';} ?> value="5">Kabupaten Deli Serdang</option>
    <option name="kab_kota" <?php if($kab_kota=='6'){echo'selected';} ?> value="6">Kabupaten Sleman</option>
    <option name="kab_kota" <?php if($kab_kota=='7'){echo'selected';} ?> value="7">Kota Yogyakarta</option>
    <option name="kab_kota" <?php if($kab_kota=='8'){echo'selected';} ?> value="8">Kabupaten Alor</option>
    <option name="kab_kota" <?php if($kab_kota=='9'){echo'selected';} ?> value="9">Kota Kupang</option>
	</select><br>
    		<label class="control-label col-sm-2">Kecamatan </label>
	<select id="kecamatan" name="kecamatan"> 
	<option name="kecamatan" <?php if($kecamatan=='1'){echo'selected';} ?> value="1">Johan Pahlawan</option>
    <option name="kecamatan" <?php if($kecamatan=='2'){echo'selected';} ?> value="2">Samatiga</option>
    <option name="kecamatan" <?php if($kecamatan=='3'){echo'selected';} ?> value="3">Bubon</option>
    <option name="kecamatan" <?php if($kecamatan=='4'){echo'selected';} ?> value="4">Blang Jerango</option>
    <option name="kecamatan" <?php if($kecamatan=='5'){echo'selected';} ?> value="5">Blang Kejeren</option>
    <option name="kecamatan" <?php if($kecamatan=='6'){echo'selected';} ?> value="6">Blang Pegayon</option>
    <option name="kecamatan" <?php if($kecamatan=='7'){echo'selected';} ?> value="7">Baiturrahman</option>
    <option name="kecamatan" <?php if($kecamatan=='8'){echo'selected';} ?> value="8">Banda Raya</option>
    <option name="kecamatan" <?php if($kecamatan=='9'){echo'selected';} ?> value="9">Kuta Alam</option>
    <option name="kecamatan" <?php if($kecamatan=='10'){echo'selected';} ?> value="10">Medan Johor</option>
    <option name="kecamatan" <?php if($kecamatan=='11'){echo'selected';} ?> value="11">Medan Kota</option>
    <option name="kecamatan" <?php if($kecamatan=='12'){echo'selected';} ?> value="12">Deli Tua</option>
    <option name="kecamatan" <?php if($kecamatan=='13'){echo'selected';} ?> value="13">Namo Rambe</option>
    <option name="kecamatan" <?php if($kecamatan=='14'){echo'selected';} ?> value="14">Ngaglik</option>
    <option name="kecamatan" <?php if($kecamatan=='15'){echo'selected';} ?> value="15">Ngemplak</option>
    <option name="kecamatan" <?php if($kecamatan=='16'){echo'selected';} ?> value="16">Kotagede</option>
    <option name="kecamatan" <?php if($kecamatan=='17'){echo'selected';} ?> value="17">Kraton</option>
    <option name="kecamatan" <?php if($kecamatan=='18'){echo'selected';} ?> value="18">Alor Barat Daya</option>
    <option name="kecamatan" <?php if($kecamatan=='19'){echo'selected';} ?> value="19">Alor Barat Laut</option>
    <option name="kecamatan" <?php if($kecamatan=='20'){echo'selected';} ?> value="20">Alak</option>
    <option name="kecamatan" <?php if($kecamatan=='21'){echo'selected';} ?> value="21">Kelapa Lima</option>
	</select><br>		
		<label class="control-label col-sm-2">Desa/Kelurahan</label>
	<select id="desa_kel" name="desa_kel"> 
	<option name="desa_kel" <?php if($desa_kel=='1'){echo'selected';} ?> value="1">Panggong</option>
    <option name="desa_kel" <?php if($desa_kel=='2'){echo'selected';} ?> value="2">Lapang</option>
    <option name="desa_kel" <?php if($desa_kel=='3'){echo'selected';} ?> value="3">Blang Beurandang</option>
    <option name="desa_kel" <?php if($desa_kel=='4'){echo'selected';} ?> value="4">Kuala Bubon</option>
    <option name="desa_kel" <?php if($desa_kel=='5'){echo'selected';} ?> value="5">Cot Seumeureung</option>
    <option name="desa_kel" <?php if($desa_kel=='6'){echo'selected';} ?> value="6">Leuken</option>
    <option name="desa_kel" <?php if($desa_kel=='7'){echo'selected';} ?> value="7">Alue Bakong</option>
    <option name="desa_kel" <?php if($desa_kel=='8'){echo'selected';} ?> value="8">Alue Lhok</option>
    <option name="desa_kel" <?php if($desa_kel=='9'){echo'selected';} ?> value="9">Beurawang</option>
    <option name="desa_kel" <?php if($desa_kel=='10'){echo'selected';} ?> value="10">Akul</option>
    <option name="desa_kel" <?php if($desa_kel=='11'){echo'selected';} ?> value="11">Gegarang</option>
    <option name="desa_kel" <?php if($desa_kel=='12'){echo'selected';} ?> value="12">Agusen</option>
    <option name="desa_kel" <?php if($desa_kel=='13'){echo'selected';} ?> value="13">Bacang</option>
    <option name="desa_kel" <?php if($desa_kel=='14'){echo'selected';} ?> value="14">Akang Siwah</option>
    <option name="desa_kel" <?php if($desa_kel=='15'){echo'selected';} ?> value="15">Anak Reje</option>
    <option name="desa_kel" <?php if($desa_kel=='16'){echo'selected';} ?> value="16">Ateuk Jawo</option>
    <option name="desa_kel" <?php if($desa_kel=='17'){echo'selected';} ?> value="17">Ateuk Pahlawan</option>
    <option name="desa_kel" <?php if($desa_kel=='18'){echo'selected';} ?> value="18">Lam Ara</option>
    <option name="desa_kel" <?php if($desa_kel=='19'){echo'selected';} ?> value="19">Lam Peuot</option>
    <option name="desa_kel" <?php if($desa_kel=='20'){echo'selected';} ?> value="20">Peunayong</option>
    <option name="desa_kel" <?php if($desa_kel=='21'){echo'selected';} ?> value="21">Laksana</option>
    <option name="desa_kel" <?php if($desa_kel=='22'){echo'selected';} ?> value="22">Kedai Durian</option>
    <option name="desa_kel" <?php if($desa_kel=='23'){echo'selected';} ?> value="23">Titi Kuning</option>
	</select><br>				
		<label class="control-label col-sm-2">Status</label>
		<select id="status" name="status">
   		<option name="status" <?php if($status=='1'){echo'selected';} ?> value="1">Dikirim ke BPBD</option>
   		<option name="status" <?php if($status=='2'){echo'selected';} ?> value="2">Laporan Ditolak</option>
    	<option name="status" <?php if($status=='3'){echo'selected';} ?> value="3">Diproses oleh Dinas</option>
    	<option name="status" <?php if($status=='4'){echo'selected';} ?> value="4">Laporan Selesai</option>
    	</select><br>
		</form>

	<div class="footer">
 	<p>Copyright &copy Alpha 2018.</p>
	</div>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
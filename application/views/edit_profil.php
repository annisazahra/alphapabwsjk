<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href=../css/icon/open-iconic-bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/edit_profil.css">

    <title>Pengaturan Akun</title>
  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a id="judul" class="navbar-brand" href="home_login.php">LACAK</a>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a id="menu" class="nav-item nav-link" href="buat_laporan.php">Buat Laporan<span class="sr-only">(current)</span></a>
      <a id="menu" class="nav-item nav-link" href="laporanku.php">Laporanku</a>
      <a id="menu" class="nav-item nav-link" href="pengaturan.php">Pengaturan Akun</a>
      <a id="menu" class="nav-item nav-link" href="about_login.php">Tentang</a>
      <a id="menu" class="nav-item nav-link" href="Home.php">Keluar</a>
    </div>
  </div>
  <p>NamaAkun</p>
</nav>

<div class="konten">
  <h4 class="setting">Edit Profil</h4>
  <form action="home_login.php" method="post">
    <div class="biodata">
      <img class="profil" src="../img/download.png">
      <a id="edit" class="btn btn-default" href="edit_profil.php">Edit Profil</a>
      <li>
        <img class="icon" src="../icon/baseline_account_circle_black_18dp.png">
        <input type="text" name="nama" >
      </li>
      <li>
        <img class="icon" src="../icon/baseline_place_black_18dp.png">
        <textarea name="alamat" class="alamat"></textarea>
      </li>
      <li>
        <img class="icon" src="../icon/baseline_email_black_18dp.png">
        <input type="email" name="email">
      <li>
        <img class="icon" src="../icon/baseline_call_black_18dp.png">
        <input type="phone" name="phone">
      </li>  
    </div>
    <button id="tombol" type="submit" class="btn btn-default">Simpan</button>
  </form>
</div>






<div class="footer">
  <p>Copyright &copy Alpha 2018.</p>
</div>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
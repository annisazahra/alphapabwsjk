<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Statistik</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/b-statistik.css">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <h6><?php echo $this->session->userdata('username'); ?></h6>

       <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item">
            <a class="navbar-brand" id="lacak" href="<?php echo base_url().'index.php/bpbd/get_bpbd';?>">LACAK</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/bpbd/get_bpbd';?>">Cek Laporan<span class="sr-only">(current)</span></a>
          </li>
         
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/bpbd/indexchart';?>">Statistik</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/bpbd/tentang';?>p">Tentang</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/alphacrud/logout';?>">Keluar</a>
          </li>
        </ul>
      </div>
    </nav>

    <div class="container">
      <ul class="nav nav-pills nav-fill">
        <li class="nav-item">
          <a class="nav-link" id="cek" href="<?php echo base_url().'index.php/bpbd/get_bpbd';?>">Cek Laporan</a>
        </li>
       
        <li class="nav-item">
          <a class="nav-link active" id="statistik" href="<?php echo base_url().'index.php/bpbd/indexchart';?>">Statistik</a>
        </li>
      </ul><br><br>
      <h2>Statistik Laporan dari Seluruh Indonesia</h2>
      
<center>
  <form>
    <table>
     <tr>

   <div id="chart_div"></div>

   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!--  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
  <canvas id="popChart" width="800" height="200" style="margin-top: 50px;">
<script>
var ctx = document.getElementById("popChart").getContext('2d');

var data_nama = [];
var data_jumlah = [];

$.post("<?php  echo base_url('index.php/alphacrud/getData') ?> ",
function (data){
  var obj = JSON.parse(data);
  $.each(obj, function(test,item){
    data_nama.push(item.jenisInfra);
    data_jumlah.push(item.jumlah);
  });


var lineChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: data_nama,
        datasets: [{
            label: 'Jumlah Laporan',
            data: data_jumlah,
            backgroundColor: 
                'rgba(17, 28, 238, 0.55)',
             
            borderColor: 
                'rgba(17, 28, 238, 0.55)',
               
            pointBorderColor:"rgba(17, 28, 238, 0.6)",
            pointBackgroundColor:"rgba(38,185,154,0.7)",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor:"rgba(220,220,220,1)",   
         
            pointBorderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
  });
});
</script>

</center>

    </div>
    
    <footer class="fixed-bottom">&copy 2018 lacak.co.id All Rights Reserved</footer>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="../js/popup.js"></script>
  </body>
</html>
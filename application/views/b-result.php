<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Home</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/b_beranda.css">

  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <h6><?php echo $this->session->userdata('username'); ?></h6>

      <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item">
            <a class="navbar-brand" id="lacak" href="<?php echo base_url().'index.php/bpbd/get_bpbd';?>">LACAK</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="<?php echo base_url().'index.php/bpbd/get_bpbd';?>">Cek Laporan<span class="sr-only">(current)</span></a>
          </li>
         
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/bpbd/indexchart';?>">Statistik</a>
          </li>
         
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/bpbd/tentang';?>">Tentang</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/alphacrud/logout';?>">Keluar</a>
          </li>
        </ul>
      </div>
    </nav>

    <!-- <div class="container">
      <h1>Home BPBD</h1>
    </div> -->
<div class="container">

      <ul class="nav nav-pills nav-fill">
        <li class="nav-item">
          <a class="nav-link active" id="cek" href="<?php echo base_url().'index.php/bpbd/get_bpbd';?>">Cek Laporan</a>
        </li>
       
        <li class="nav-item">
          <a class="nav-link" id="statistik" href="<?php echo base_url().'index.php/bpbd/indexchart';?>">Statistik</a>
        </li>
      </ul>
    <br><br>
                <h5><a class="kembali" style="color: #C0392B;" href="<?php echo base_url().'index.php/bpbd/get_bpbd';?>">&larr; kembali</a></h5><br>

      <center>

      <div class="row" id="laporan">
        <!-- <div class="col-3">
          <img class="gambar" src="../img/lake.jpg">
        </div> -->
        <div class="col-12">
         <!--  <h5>Infrastruktur</h5>
          <h6>Status</h6> -->
          <h3>Hasil Pencarian</h3>
          <table class="table">
            <tr>
              <th>Nomor Laporan</th>
              <th>Nama Infrastruktur</th>
              <th>Status</th>
              <th>Lihat Laporan</th>
            </tr>
<?php
 
    if(count($cari)>0)
    {
      foreach ($cari as $data) {?>
     <!--  echo $data->nomor_laporan . " => " . $data->nama_infrastruktur ."<br>";
      -->
      <tr>
      <td><?php 
      echo $data->nomor_laporan;
     $nomor_laporan = $data->nomor_laporan ?></td>
      <td><?php echo $data->nama_infrastruktur; ?></td>
      <td>
      <?php 
      $status = $data->status;
      if ($status=="1"){
      echo "Dikirim ke BPBD";
      }elseif ($status=="2"){
      echo "Laporan Ditolak";
      }elseif ($status=="3"){
      echo "Diproses oleh Dinas";
      }elseif ($status=="4"){
      echo "Selesai";
      } ?>
     </td>
      <td> <div class="col-7">
          <a class="lihat" href="<?php echo base_url('index.php/bpbd/editbpbd/'.$data->nomor_laporan)?>">Lihat Laporan</a>
        </div></td>
    
    </tr>
    <?php } ?>
     <?php } 
 
    else
    {
      echo "Data tidak ditemukan";
    }
 
    ?>
</table>

        </div>
      </div>
    </div>
        </center>
      </div>

    <footer class="fixed-bottom">&copy 2018 lacak.co.id All Rights Reserved</footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
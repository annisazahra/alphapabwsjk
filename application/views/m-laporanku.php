<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Laporanku</title>
    <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/mlaporanku.css">  
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <h6>Pelapor</h6>

      <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item">
            <a class="navbar-brand" id="lacak" href="<?php echo base_url().'index.php/pelapor/get_laporanku';?>">LACAK</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/pelapor/get_laporanku';?>">Laporanku<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/pelapor/mbuatlaporan';?>">Buat Laporan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="">Pengaturan Akun</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="">Tentang</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/alphacrud/logout';?>">Keluar</a>
          </li>
        </ul>
      </div>
    </nav>

    <div class="container">
      <ul class="nav nav-pills nav-fill">
        <li class="nav-item">
          <a class="nav-link active" id="laporanku" href="<?php echo base_url().'index.php/pelapor/get_laporanku';?>">Laporanku</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="buat" href="<?php echo base_url().'index.php/pelapor/mbuatlaporan';?>">Buat Laporan</a>
        </li>
      </ul>
      <div class="row">
        <form class="cari" action="<?php echo base_url('index.php/pelapor/hasil')?>" action="GET">
          <div class="input-group mb-3">
            <input type="text" name="cari" class="form-control" id="cari" placeholder="Pencarian">
            <div class="input-group-append">
              <button class="btn btn-primary" type="submit">Cari</button>
            </div>
          </div>
        </form>
        <form class="urutkan" action="<?php echo base_url('index.php/pelapor/urutkan')?>" action="GET">
          <div class="input-group mb-3">
            <select name="urutkan" id="urutkan" class="form-control">
              <option value="" selected>Urut Berdasarkan</option>
              <option value="nomor_laporan">Nomor Laporan</option>
              <option value="created_date">Tanggal Laporan</option>
              <option value="nama_infrastruktur">Nama Infrastruktur</option>
              <option value="status">Status</option>
            </select>
            <div class="input-group-append">
              <button class="btn btn-primary" type="submit">Urutkan</button>
            </div>
          </div>
        </form>
      </div>
 <table class="table">
  <tr>
      <th>Nomor Laporan</th>
      <th>Nama Infrastruktur</th>
      <th>Status laporan</th>
      <th>Lihat Data</th>
    </tr>
        <?php
    foreach ($dataaaa as $r){ ?>
    
    <tr>
      
       <td><?php echo $r->nomor_laporan;
     $nomor_laporan = $r->nomor_laporan
      ?></td>
      <td><?php echo $r->nama_infrastruktur; ?></td>
      <td><?php 
      $status = $r->status;
      if ($status=="1"){
      echo "Dikirim ke BPBD";
      }elseif ($status=="2"){
      echo "Laporan Ditolak";
      }elseif ($status=="3"){
      echo "Diproses oleh Dinas";
      }elseif ($status=="4"){
      echo "Selesai";
      }
      ?></td>
      <td>
     
          <a class="lihat" style="color: #fff; text-decoration: none; background-color: #C0392B;padding: 3px 5px 3px 5px;border-radius: 5px;"href="<?php echo base_url('index.php/pelapor/edit/'.$r->nomor_laporan)?>">Lihat Laporan</a>
       
      </td>
    
    </tr>
    <?php } ?>
      </table>
    </div>
  </div>
</div>

  <div class="modal fade" id="form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-header">
        <h2>Detail Laporan</h2>
      </div>
      <table class="table">

        <tr>
          <td>Nama Infrastruktur</td>
          <td>:</td>
          <td><input type="" name="" value=""></td>
        </tr>
          <tr>
          <td>Bencana Penyebab</td>
          <td>:</td>

        </tr>
          <tr>
          <td>Deskripsi Kerusakan</td>
          <td>:</td>

        </tr>
        <tr>
          <td>Jenis Infrastruktur</td>
          <td>:</td>
        
        </tr>
        <tr>
          <td>Tingkat Kerusakan</td>
          <td>:</td>
          
        </tr>
          <tr>
          <td>Alamat Infrastruktur</td>
          <td>:</td>
        </tr>
        <tr>
          <td>Provinsi</td>
          <td>:</td>
         
        </tr>
        <tr>
          <td>Kabupaten/Kota</td>
          <td>:</td>
          
        </tr>
        <tr>
          <td>Kecamatan</td>
          <td>:</td>
        </tr>
        <tr>
          <td>Desa/Kelurahan</td>
          <td>:</td>
        </tr>
        <tr>
          <td></td>
          <td>
            <button type="button" data-dismiss="modal" class="btn btn-secondary">Kembali</button>

          </td>
        </tr>
      </table>
    </div>
  </div>
</div>

    <!--   <div class="row" id="laporanku">
        <div class="col-3">
          <img class="gambar" src="../img/download.png">
        </div>
        <div class="col-5">
          <h4>Infrastruktur</h4>
          <h5>Tingkatan</h5>
          <h5>Status</h5>
        </div>
        <div class="col-4">
          <a class="lihat" href="m-lihatlaporan.php">Lihat Laporan</a>
        </div>
      </div> -->
     




    </div>
    
    <footer class="fixed-bottom">&copy 2018 lacak.co.id All Rights Reserved</footer>

    <!-- <div class="chatbox chatbox--tray chatbox--empty">
        <div class="chatbox__title">
            <h5><a href="#">Dinas</a></h5>
            <button class="chatbox__title__tray">
                <span></span>
            </button>
            <button class="chatbox__title__close">
                <span>
                    <svg viewBox="0 0 12 12" width="12px" height="12px">
                        <line stroke="#FFFFFF" x1="11.75" y1="0.25" x2="0.25" y2="11.75"></line>
                        <line stroke="#FFFFFF" x1="11.75" y1="11.75" x2="0.25" y2="0.25"></line>
                    </svg>
                </span>
            </button>
        </div>
        <div class="chatbox__body">
            <div class="chatbox__body__message chatbox__body__message--left">
                <img src="https://s3.amazonaws.com/uifaces/faces/twitter/brad_frost/128.jpg" alt="Picture">
                <p>Curabitur consequat nisl suscipit odio porta, ornare blandit ante maximus.</p>
            </div>
            <div class="chatbox__body__message chatbox__body__message--right">
                <img src="https://s3.amazonaws.com/uifaces/faces/twitter/arashmil/128.jpg" alt="Picture">
                <p>Cras dui massa, placerat vel sapien sed, fringilla molestie justo.</p>
            </div>
            <div class="chatbox__body__message chatbox__body__message--right">
                <img src="https://s3.amazonaws.com/uifaces/faces/twitter/arashmil/128.jpg" alt="Picture">
                <p>Praesent a gravida urna. Mauris eleifend, tellus ac fringilla imperdiet, odio dolor sodales libero, vel mattis elit mauris id erat. Phasellus leo nisi, convallis in euismod at, consectetur commodo urna.</p>
            </div>
        </div>
        <form class="chatbox__credentials">
            <div class="form-group">
                <label for="inputName">Name:</label>
                <input type="text" class="form-control" id="inputName" required>
            </div>
            <div class="form-group">
                <label for="inputEmail">Email:</label>
                <input type="email" class="form-control" id="inputEmail" required>
            </div>
            <button type="submit" class="btn btn-success btn-block">Enter Chat</button>
        </form>
        <textarea class="chatbox__message" placeholder="Write something interesting"></textarea>
    </div>
 -->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="../js/popup.js"></script>
  </body>
</html>
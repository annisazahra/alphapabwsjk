<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/cek_laporan.css">

    <title>Cek Laporan</title>
  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a id="judul" class="navbar-brand" href="home_dinas.php">LACAK</a>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a id="menu" class="nav-item nav-link" href="cek_laporan.php">Cek Laporan<span class="sr-only">(current)</span></a>
      <a id="menu" class="nav-item nav-link" href="statistik.php">Statistik</a>
      <a id="menu" class="nav-item nav-link" href="pengaturan.php">Pengaturan Akun</a>
      <a id="menu" class="nav-item nav-link" href="about_login.php">Tentang</a>
      <a id="menu" class="nav-item nav-link" href="Home.php">Keluar</a>
    </div>
  </div>
  <p>NamaAkun</p>
</nav>

<div class="bar">
    <ul class="nav nav-pills nav-fill" id="pills-tab" role="tablist">
      <li class="nav-item">
        <a id="cek" class="nav-link active" id="pills-home-tab" aria-controls="pills-home" aria-selected="true" href="cek_laporan.php">Cek Laporan</a>
      </li>
      <li class="nav-item">
        <a id="stat" class="nav-link" id="pills-profile-tab" aria-controls="pills-profile" aria-selected="true" href="statistik.php">Statistik</a>
      </li>
      </ul>
  </div>

<div class="container">
  <div class="row" id="filter-sort">
    <div class="col-md-3"></div>
    <div class="col-md-6">
      <div class="form-group col-md-10">
        <div class="input-group">
          <select class="custom-select">
            <option selected value="">Urutkan Berdasarkan</option>
            <option value="1">One</option>
            <option value="2">Two</option>
            <option value="3">Three</option>
          </select>
          <div class="input-group-append">
            <button class="btn btn-primary" id="tombol-sort" type="button">Urutkan</button>
          </div>
        </div>
      </div>
      <div class="form-group col-md-10">
        <div class="input-group">
          <select class="custom-select">
            <option selected value="">Filter Berdasarkan</option>
            <option value="1">One</option>
            <option value="2">Two</option>
            <option value="3">Three</option>
          </select>
          <div class="input-group-append">
            <button class="btn btn-primary" id="tombol-filter" type="button">Filter</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-3"></div>

  <div class="row" id="laporan">
    <div class="col-4">
      <img class="gambar" src="<?php echo base_url(); ?>/img/download.png">
    </div>
    <div class="col-4">
      <a class="nama" href="lihat_laporan_dinas.php">Nama Infrastruktur</a>
      <p class="status">Status</p>
    </div>
    <div class="col-4">
        <button id="lihat" class="btn btn-primary" data-toggle="modal" data-target="#ubahStatus"  \>Ubah Status</button>
    </div>
  </div>
  <hr>
</div>

<div class="modal fade" id="ubahStatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form action="">
        <div class="modal-body">
          <center><h4>Ubah Status</h4></center>
          <ul>
            <li>
              <input type="radio" name="status" id="proses">
              <label for="proses">Diproses oleh Dinas</label>
            </li>
            <li>
              <input type="radio" name="status" id="selesai">
              <label for="selesai">Selesai Diperbaiki</label>
            </li>
          </ul>
          <div class="modal-footer">
            <button type="button" id="ubah" class="btn btn-primary">Ubah</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>



<div class="footer">
  <p>Copyright &copy Alpha 2018.</p>
</div>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
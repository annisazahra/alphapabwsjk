<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Tentang</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/d-tentang.css">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <h6>Nama Akun</h6>

      <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item">
            <a class="navbar-brand" id="lacak" href="d-beranda.php">LACAK</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="d-ceklaporan.php">Cek Laporan<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="d-statistik.php">Statistik</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="d-chat.php">Obrolan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="d-tentang.php">Tentang</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="beranda.php">Keluar</a>
          </li>
        </ul>
      </div>
    </nav>

    <div class="container">
      <h3>Lacak</h3>
      <p>
        Lacak(Lapor, Cari, Perbaiki) adalah sebuah aplikasi berbasis web yang dapat memudahkan masyarakat untuk mengirimkan laporan mengenai adanya kerusakan infrastuktur / fasilitas publik yang rusak akibat bencana. Laporan tersebut akan dikirim ke BPBD yang nantinya akan melakukan pengecekan secara langsung ke lapangan. Apabila laporan tersebut valid, maka akan diteruskan ke dinas terkait untuk perbaikan.
      </p>
    </div>

    <footer class="fixed-bottom">&copy 2018 lacak.co.id All Rights Reserved</footer>




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
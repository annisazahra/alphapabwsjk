<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/login_d.css">

    <title>Login</title>
  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a id="judul" class="navbar-brand" href="home.php">LACAK</a>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a id="menu" class="nav-item nav-link" href="masyarakat.php">Daftar <span class="sr-only">(current)</span></a>
      <a id="menu" class="nav-item nav-link" href="login.php">Masuk</a>
      <a id="menu" class="nav-item nav-link" href="about.php">Tentang</a>
      <a id="menu" class="nav-item nav-link" href="kontak.php">Kontak</a>
    </div>
  </div>
  <a id="daftar" class="btn btn-default" href="masyarakat.php">Daftar</a>
</nav>


<div class="konten">
  <a id="kembali" href="login.php">< kembali</a>
  <h1>Masuk</h1>
       <?php echo form_open('alphacrud/proses_login');?>

  <div class="form-group">
    <label for="username">Nama Pengguna</label>
    <input type="text" class="form-control" id="username" name="username" aria-describedby="emailHelp" placeholder="Nama Pengguna">
  </div>
  <div class="form-group">
    <label for="password">Kata Sandi</label>
    <input type="password" class="form-control" id="password" name="password" placeholder="Kata Sandi">
    <input type="checkbox" onclick="showPassword()">Tampilakan Kata Sandi
  </div>
  <div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="remember">
    <label class="form-check-label" for="remember">Ingat saya</label>
  </div>
  <button type="submit" id="tombol" class="btn btn-primary">Masuk</button>
   <?php if(isset($pesan)){
      echo $pesan;
    }?>
    <?php echo form_close();?>
</div>


<div class="footer">
  <p>Copyright &copy Alpha 2018.</p>
</div>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="../js/password.js"></script>
  </body>
</html>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Lihat Laporan</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/b-lihatlaporan.css">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <h6><?php echo $this->session->userdata('username'); ?></h6>

      <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item">
            <a class="navbar-brand" id="lacak" href="<?php echo base_url().'index.php/bpbd/get_bpbd';?>">LACAK</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/bpbd/get_bpbd';?>">Cek Laporan<span class="sr-only">(current)</span></a>
          </li>
         
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/bpbd/indexchart';?>">Statistik</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/bpbd/tentang';?>p">Tentang</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/alphacrud/logout';?>">Keluar</a>
          </li>
        </ul>
      </div>
    </nav>

    <div class="container">
      <ul class="nav nav-pills nav-fill">
        <li class="nav-item">
          <a class="nav-link active" id="cek" href="<?php echo base_url().'index.php/bpbd/get_bpbd';?>">Cek Laporan</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="statistik" href="<?php echo base_url().'index.php/bpbd/indexchart';?>">Statistik</a>
        </li>
      </ul>
</div>

    <?php if($dataEdit){
     $nomor_laporan=$dataEdit->nomor_laporan;
    $nama_infrastruktur=$dataEdit->nama_infrastruktur;
    $bencana_penyebab=$dataEdit->bencana_penyebab;
    $jenis_infra=$dataEdit->jenis_infra;
    $status=$dataEdit->status;
    $desc_kerusakan=$dataEdit->desc_kerusakan;
    $alamat_infra=$dataEdit->alamat_infra;
    $provinsi=$dataEdit->provinsi;
    $kab_kota=$dataEdit->kab_kota;
    $kecamatan=$dataEdit->kecamatan;
    $desa_kel=$dataEdit->desa_kel;
    $tingkat_kerusakan=$dataEdit->tingkat_kerusakan;
    $dinas=$dataEdit->dinas;
    $url=$dataEdit->url;
    $username=$dataEdit->username;
    $tanggallaporan=$dataEdit->created_date;

}else{
  $tanggallaporan="";
  $username="";
  $nama_infrastruktur="";
  $bencana_penyebab="";
  $jenis_infra="";
  $status="";
  $desc_kerusakan="";
  $alamat_infra="";
  $provinsi="";
  $kab_kota="";
  $kecamatan="";
  $desa_kel="";
  $tingkat_kerusakan="";
  $dinas="";
  $url="";
} ?>
  
<h5><a class="kembali" href="<?php echo base_url().'index.php/bpbd/get_bpbd';?>">&larr; kembali</a></h5>
  <form method="POST" action="<?php echo site_url('bpbd/update/'.$nomor_laporan) ?>">
    <?php 
    if(file_exists($url)){
      ?> 
      <img src="<?php echo base_url($url)?>" alt="" class="featurImg" width="300px"><?php 
    }?> <br>
    <br>
    <br>
    <div class="form-group">
    <label class="control-label col-sm-2">Nomor Laporan</label>
    <input type="text" name="nomor_laporan" value="<?php echo $nomor_laporan?>" class="form-control input-xs control-label col-sm-4" readonly><br>
    <label class="control-label col-sm-2">Tanggal Laporan</label>
    <input type="text" name="tanggallaporan" value="<?php echo $tanggallaporan?>" class="form-control input-xs control-label col-sm-4" readonly><br>
     <label class="control-label col-sm-2">Email Pelapor</label>
    <input type="text" name="username" value="<?php echo $username?>" class="form-control input-xs control-label col-sm-4" readonly><br>
    <label class="control-label col-sm-2">Nama Infrastruktur</label>
    <input type="text" name="nama_infrastruktur" value="<?php echo $nama_infrastruktur?>" class="form-control input-xs control-label col-sm-4" readonly><br>
    <label class="control-label col-sm-2">Bencana Penyebab</label>
    <input type="text" name="bencana_penyebab" value="<?php echo $bencana_penyebab?>" class="form-control input-xs control-label col-sm-4" readonly><br>
    <label class="control-label col-sm-2">Jenis Infrastruktur </label>
    <select id="jenis_infra" name="jenis_infra" class="form-control input-xs control-label col-sm-4" readonly>
    <option name="jenis_infra" <?php if($jenis_infra=='1'){echo'selected';} ?> value="1">Puskesmas</option>
    <option name="jenis_infra" <?php if($jenis_infra=='2'){echo'selected';} ?> value="2">Jembatan</option>
    <option name="jenis_infra" <?php if($jenis_infra=='3'){echo'selected';} ?> value="3">Rumah Sakit</option>
    <option name="jenis_infra" <?php if($jenis_infra=='4'){echo'selected';} ?> value="4">Jalan Raya</option>
    <option name="jenis_infra" <?php if($jenis_infra=='5'){echo'selected';} ?> value="5">Jalan Tol</option>
    <option name="jenis_infra" <?php if($jenis_infra=='6'){echo'selected';} ?> value="6">Jalur Rel Kereta Api</option>
    <option name="jenis_infra" <?php if($jenis_infra=='7'){echo'selected';} ?> value="7">Lembaga Pemasyarakatan</option>
    <option name="jenis_infra" <?php if($jenis_infra=='8'){echo'selected';} ?> value="8">Perkantoran</option>
    <option name="jenis_infra" <?php if($jenis_infra=='9'){echo'selected';} ?> value="9">Jalan</option>
    <option name="jenis_infra" <?php if($jenis_infra=='10'){echo'selected';} ?> value="10">Terowongan</option>
    <option name="jenis_infra" <?php if($jenis_infra=='11'){echo'selected';} ?> value="11">Lampu Jalan</option>
    <option name="jenis_infra" <?php if($jenis_infra=='12'){echo'selected';} ?> value="12">Rambu Lalu Lintas</option>
    <option name="jenis_infra" <?php if($jenis_infra=='13'){echo'selected';} ?> value="13">Pelabuhan</option>
    <option name="jenis_infra" <?php if($jenis_infra=='14'){echo'selected';} ?> value="14">Bandar Udara</option>
    <option name="jenis_infra" <?php if($jenis_infra=='15'){echo'selected';} ?> value="15">Taman</option>
    <option name="jenis_infra" <?php if($jenis_infra=='16'){echo'selected';} ?> value="16">Sekolah Negeri</option>
    <option name="jenis_infra" <?php if($jenis_infra=='17'){echo'selected';} ?> value="17">Universitas Negeri</option>
    <option name="jenis_infra" <?php if($jenis_infra=='18'){echo'selected';} ?> value="18">Stadion</option>
    </select><br>
    <label class="control-label col-sm-2">Tingkat Kerusakan </label>
    <select name="tingkat_kerusakan" id="tingkat_kerusakan" class="form-control input-xs control-label col-sm-4" readonly>
    <option name="tingkat_kerusakan" <?php if($tingkat_kerusakan=='1'){echo'selected';} ?> value="1">Ringan</option>
    <option name="tingkat_kerusakan" <?php if($tingkat_kerusakan=='2'){echo'selected';} ?> value="2">Sedang</option>
    <option name="tingkat_kerusakan" <?php if($tingkat_kerusakan=='3'){echo'selected';} ?> value="3">Parah</option>
    </select><br>
    <label class="control-label col-sm-2">Deskripsi Kerusakan </label>
    <input type="text" name="desc_kerusakan" value="<?php echo $desc_kerusakan?>" class="form-control input-xs control-label col-sm-4" readonly><br>
    <label class="control-label col-sm-2">Alamat Infrastruktur </label>
    <input type="text" name="alamat_infra" value="<?php echo $alamat_infra?>"class="form-control input-xs control-label col-sm-4" readonly><br>
    <label class="control-label col-sm-2">Provinsi</label>
   <select id="provinsi" name="provinsi" class="form-control input-xs control-label col-sm-4" readonly>
    <option name="provinsi" <?php if($provinsi=='1'){echo'selected';} ?> value="1">Aceh</option>
    <option name="provinsi" <?php if($provinsi=='2'){echo'selected';} ?> value="2">Sumatera Utara</option>
    <option name="provinsi" <?php if($provinsi=='3'){echo'selected';} ?> value="3">DI Yogyakarta</option>
    <option name="provinsi" <?php if($provinsi=='4'){echo'selected';} ?> value="4">NTT</option>
    </select><br>
    <label class="control-label col-sm-2">Kabupaten/Kota </label>
<select id="kab_kota" name="kab_kota" class="form-control input-xs control-label col-sm-4" readonly> 
  <option name="kab_kota" <?php if($kab_kota=='1'){echo'selected';} ?> value="1">Kabupaten Aceh Barat</option>
    <option name="kab_kota" <?php if($kab_kota=='2'){echo'selected';} ?> value="2">Kabupaten Gayo Lues</option>
    <option name="kab_kota" <?php if($kab_kota=='3'){echo'selected';} ?> value="3">Kota Banda Aceh</option>
    <option name="kab_kota" <?php if($kab_kota=='4'){echo'selected';} ?> value="4">Kota Medan</option>
    <option name="kab_kota" <?php if($kab_kota=='5'){echo'selected';} ?> value="5">Kabupaten Deli Serdang</option>
    <option name="kab_kota" <?php if($kab_kota=='6'){echo'selected';} ?> value="6">Kabupaten Sleman</option>
    <option name="kab_kota" <?php if($kab_kota=='7'){echo'selected';} ?> value="7">Kota Yogyakarta</option>
    <option name="kab_kota" <?php if($kab_kota=='8'){echo'selected';} ?> value="8">Kabupaten Alor</option>
    <option name="kab_kota" <?php if($kab_kota=='9'){echo'selected';} ?> value="9">Kota Kupang</option>
  </select><br>
      <label class="control-label col-sm-2">Kecamatan </label>
<select id="kecamatan" name="kecamatan" class="form-control input-xs control-label col-sm-4" readonly> 
  <option name="kecamatan" <?php if($kecamatan=='1'){echo'selected';} ?> value="1">Johan Pahlawan</option>
    <option name="kecamatan" <?php if($kecamatan=='2'){echo'selected';} ?> value="2">Samatiga</option>
    <option name="kecamatan" <?php if($kecamatan=='3'){echo'selected';} ?> value="3">Bubon</option>
    <option name="kecamatan" <?php if($kecamatan=='4'){echo'selected';} ?> value="4">Blang Jerango</option>
    <option name="kecamatan" <?php if($kecamatan=='5'){echo'selected';} ?> value="5">Blang Kejeren</option>
    <option name="kecamatan" <?php if($kecamatan=='6'){echo'selected';} ?> value="6">Blang Pegayon</option>
    <option name="kecamatan" <?php if($kecamatan=='7'){echo'selected';} ?> value="7">Baiturrahman</option>
    <option name="kecamatan" <?php if($kecamatan=='8'){echo'selected';} ?> value="8">Banda Raya</option>
    <option name="kecamatan" <?php if($kecamatan=='9'){echo'selected';} ?> value="9">Kuta Alam</option>
    <option name="kecamatan" <?php if($kecamatan=='10'){echo'selected';} ?> value="10">Medan Johor</option>
    <option name="kecamatan" <?php if($kecamatan=='11'){echo'selected';} ?> value="11">Medan Kota</option>
    <option name="kecamatan" <?php if($kecamatan=='12'){echo'selected';} ?> value="12">Deli Tua</option>
    <option name="kecamatan" <?php if($kecamatan=='13'){echo'selected';} ?> value="13">Namo Rambe</option>
    <option name="kecamatan" <?php if($kecamatan=='14'){echo'selected';} ?> value="14">Ngaglik</option>
    <option name="kecamatan" <?php if($kecamatan=='15'){echo'selected';} ?> value="15">Ngemplak</option>
    <option name="kecamatan" <?php if($kecamatan=='16'){echo'selected';} ?> value="16">Kotagede</option>
    <option name="kecamatan" <?php if($kecamatan=='17'){echo'selected';} ?> value="17">Kraton</option>
    <option name="kecamatan" <?php if($kecamatan=='18'){echo'selected';} ?> value="18">Alor Barat Daya</option>
    <option name="kecamatan" <?php if($kecamatan=='19'){echo'selected';} ?> value="19">Alor Barat Laut</option>
    <option name="kecamatan" <?php if($kecamatan=='20'){echo'selected';} ?> value="20">Alak</option>
    <option name="kecamatan" <?php if($kecamatan=='21'){echo'selected';} ?> value="21">Kelapa Lima</option>
  </select><br>
      <label class="control-label col-sm-2">Desa/Kelurahan</label>
<select id="desa_kel" name="desa_kel" class="form-control input-xs control-label col-sm-4" readonly> 
  <option name="desa_kel" <?php if($desa_kel=='1'){echo'selected';} ?> value="1">Panggong</option>
    <option name="desa_kel" <?php if($desa_kel=='2'){echo'selected';} ?> value="2">Lapang</option>
    <option name="desa_kel" <?php if($desa_kel=='3'){echo'selected';} ?> value="3">Blang Beurandang</option>
    <option name="desa_kel" <?php if($desa_kel=='4'){echo'selected';} ?> value="4">Kuala Bubon</option>
    <option name="desa_kel" <?php if($desa_kel=='5'){echo'selected';} ?> value="5">Cot Seumeureung</option>
    <option name="desa_kel" <?php if($desa_kel=='6'){echo'selected';} ?> value="6">Leuken</option>
    <option name="desa_kel" <?php if($desa_kel=='7'){echo'selected';} ?> value="7">Alue Bakong</option>
    <option name="desa_kel" <?php if($desa_kel=='8'){echo'selected';} ?> value="8">Alue Lhok</option>
    <option name="desa_kel" <?php if($desa_kel=='9'){echo'selected';} ?> value="9">Beurawang</option>
    <option name="desa_kel" <?php if($desa_kel=='10'){echo'selected';} ?> value="10">Akul</option>
    <option name="desa_kel" <?php if($desa_kel=='11'){echo'selected';} ?> value="11">Gegarang</option>
    <option name="desa_kel" <?php if($desa_kel=='12'){echo'selected';} ?> value="12">Agusen</option>
    <option name="desa_kel" <?php if($desa_kel=='13'){echo'selected';} ?> value="13">Bacang</option>
    <option name="desa_kel" <?php if($desa_kel=='14'){echo'selected';} ?> value="14">Akang Siwah</option>
    <option name="desa_kel" <?php if($desa_kel=='15'){echo'selected';} ?> value="15">Anak Reje</option>
    <option name="desa_kel" <?php if($desa_kel=='16'){echo'selected';} ?> value="16">Ateuk Jawo</option>
    <option name="desa_kel" <?php if($desa_kel=='17'){echo'selected';} ?> value="17">Ateuk Pahlawan</option>
    <option name="desa_kel" <?php if($desa_kel=='18'){echo'selected';} ?> value="18">Lam Ara</option>
    <option name="desa_kel" <?php if($desa_kel=='19'){echo'selected';} ?> value="19">Lam Peuot</option>
    <option name="desa_kel" <?php if($desa_kel=='20'){echo'selected';} ?> value="20">Peunayong</option>
    <option name="desa_kel" <?php if($desa_kel=='21'){echo'selected';} ?> value="21">Laksana</option>
    <option name="desa_kel" <?php if($desa_kel=='22'){echo'selected';} ?> value="22">Kedai Durian</option>
    <option name="desa_kel" <?php if($desa_kel=='23'){echo'selected';} ?> value="23">Titi Kuning</option>
  </select><br>          
  </div>
  <div class="form-group"> <label class="control-label col-sm-2">Status</label>
    <select id="status" name="status" class="form-control input-xs control-label col-sm-4">
    <option name="status" <?php if($status=='1'){echo'selected';} ?> value="1">Dikirim ke BPBD</option>
    <option name="status" <?php if($status=='2'){echo'selected';} ?> value="2">Laporan Ditolak</option>
    <option name="status" <?php if($status=='3'){echo'selected';} ?> value="3">Diproses oleh Dinas</option>
    <!-- <option name="status" <?php if($status=='4'){echo'selected';} ?> value="4">Laporan Selesai</option> -->
    </select><br>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-4 ">Kirim ke</label>
    <select id="dinas" name="dinas" class="form-control input-xs control-label col-sm-4">
    <option name="dinas" <?php if($dinas==''){echo'selected';} ?>value="" >Pilih Dinas</option>
    <option name="dinas" <?php if($dinas=='dinaspendidikan'){echo'selected';} ?> value="dinaspendidikan">Dinas Pendidikan</option>
    <option name="dinas" <?php if($dinas=='dinaspekerjaanumum'){echo'selected';} ?> value="dinaspekerjaanumum">Dinas Pekerjaan Umum</option>
    </select><br>
  </div>
  <div class="form-group">
    <div class="control-label col-sm-5">
    <input type="submit" name="btnSubmit" value="Simpan"/>
  </div>
  </div>
  </form>
        </center>
      </div>
     
    </div>
    <br><br><br>
    
    <footer class="fixed-bottom">&copy 2018 lacak.co.id All Rights Reserved</footer>


    <div class="modal fade" id="identitas" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Identitas Pelapor</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <center><img class="profil" src="../img/download.png"></center>
            <h3>Nama Pelapor</h3>
            <table>
              <tr>
                <td>Email</td>
                <td>:</td>
                <td></td>
              </tr>
              <tr>
                <td>No. Telepon</td>
                <td>:</td>
                <td></td>
              </tr>
              <tr>
                <td>Alamat</td>
                <td>:</td>
                <td></td>
              </tr>
            </table>
          </div>
          <br>
          <br><br><br>
          <div class="modal-footer">
            <a href="b-chat.php" class="btn btn-primary">Kirim Pesan</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="../js/popup.js"></script>
  </body>
</html>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
     <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/laporanku.css">
    <script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-3.3.1.min.js' ?>"></script>


    <title>Laporanku</title>
  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a id="judul" class="navbar-brand" href="home_login.php">LACAK</a>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a id="menu" class="nav-item nav-link" href="<?php echo base_url().'index.php/pelapor/buatlaporan' ?>">Buat Laporan<span class="sr-only">(current)</span></a>
      <a id="menu" class="nav-item nav-link" href="laporanku.php">Laporanku</a>
      <a id="menu" class="nav-item nav-link" href="pengaturan.php">Pengaturan Akun</a>
      <a id="menu" class="nav-item nav-link" href="about_login.php">Tentang</a>
      <a id="menu" class="nav-item nav-link" href="<?php echo base_url().'index.php/alphacrud/logout' ?>">Keluar</a>
    </div>
  </div>
  <p><?php echo $this->session->userdata('username'); ?></p>
</nav>

<div class="konten">
  <div class="bar">
    <ul class="nav nav-pills nav-fill" id="pills-tab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="pills-home-tab" aria-controls="pills-home" aria-selected="true" href="laporanku.php">Laporanku</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-profile-tab" aria-controls="pills-profile" aria-selected="true" href="<?php echo base_url().'index.php/pelapor/buatlaporan' ?>">Buat Laporan</a>
    </li>
  </ul>
  </div>
</div>


<div class="container">
   <br />
   <br />
   <br />
   <h2 align="center">Live Data Search in Codeigniter using Ajax JQuery</h2><br />
   <div class="form-group">
    <div class="input-group">
     <span class="input-group-addon">Search</span>
     <input type="text" name="search_text" id="search_text" placeholder="Search by Customer Details" class="form-control" />
    </div>
   </div>
   <br />
   <div id="result"></div>
  </div>
  <div style="clear:both"></div>
  <br />
  <br />

      <table class="table">
        <?php
    foreach ($dataaaa as $r){ ?>
    <tr>
      <th>Nomor Laporan</th>
      <th>Nama Infrastruktur</th>
      <th>Status laporan</th>
      <th>Lihat Data</th>
    </tr>
    <tr>
      
       <td><?php echo $r->nomor_laporan;
     $nomor_laporan = $r->nomor_laporan
      ?></td>
      <td><?php echo $r->nama_infrastruktur; ?></td>
      <td><?php 
      $status = $r->status;
      if ($status=="1"){
      echo "Dikirim ke BPBD";
      }elseif ($status=="2"){
      echo "Laporan Ditolak";
      }elseif ($status=="3"){
      echo "Diproses oleh Dinas";
      }elseif ($status=="4"){
      echo "Selesai";
      }
      ?></td>
      
      <td><a href="<?php echo base_url('index.php/pelapor/edit/'.$r->nomor_laporan)?>" class="btn btn-primary btn-sm">Lihat Data</a></td>
    
    </tr>
    <?php } ?>
      </table>
    </div>
  </div>
</div>

  <div class="modal fade" id="form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-header">
        <h2>Detail Laporan</h2>
      </div>
      <table class="table">

        <tr>
          <td>Nama Infrastruktur</td>
          <td>:</td>
          <td><input type="" name="" value=""></td>
        </tr>
          <tr>
          <td>Bencana Penyebab</td>
          <td>:</td>

        </tr>
          <tr>
          <td>Deskripsi Kerusakan</td>
          <td>:</td>

        </tr>
        <tr>
          <td>Jenis Infrastruktur</td>
          <td>:</td>
        
        </tr>
        <tr>
          <td>Tingkat Kerusakan</td>
          <td>:</td>
          
        </tr>
          <tr>
          <td>Alamat Infrastruktur</td>
          <td>:</td>
        </tr>
        <tr>
          <td>Provinsi</td>
          <td>:</td>
         
        </tr>
        <tr>
          <td>Kabupaten/Kota</td>
          <td>:</td>
          
        </tr>
        <tr>
          <td>Kecamatan</td>
          <td>:</td>
        </tr>
        <tr>
          <td>Desa/Kelurahan</td>
          <td>:</td>
        </tr>
        <tr>
          <td></td>
          <td>
            <button type="button" data-dismiss="modal" class="btn btn-secondary">Kembali</button>

          </td>
        </tr>
      </table>
    </div>
  </div>
</div>



  
<div class="footer">
  <p>Copyright &copy Alpha 2018.</p>
</div>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
<script>
$(document).ready(function(){

 load_data();

 function load_data(query)
 {
  $.ajax({
   url:"<?php echo base_url(); ?>index.php/pelapor/fetch",
   method:"POST",
   data:{query:query},
   success:function(data){
    $('#result').html(data);
   }
  })
 }

 $('#search_text').keyup(function(){
  var search = $(this).val();
  if(search != '')
  {
   load_data(search);
  }
  else
  {
   load_data();
  }
 });
});
</script>
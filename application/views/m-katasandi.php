<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Edit Profil</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/m-editprofil.css">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <h6>Nama Akun</h6>

      <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item">
            <a class="navbar-brand" id="lacak" href="m-beranda.php">LACAK</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="m-laporanku.php">Laporanku<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="m-buatlaporan.php">Buat Laporan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="m-pengaturan.php">Pengaturan Akun</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="m-tentang.php">Tentang</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="beranda.php">Keluar</a>
          </li>
        </ul>
      </div>
    </nav>
      <div class="container">

 <?php if($dataEditProfil){
    $nama=$dataEditProfil->nama;
    $username=$dataEditProfil->username;
   
}else{
  $tanggallaporan="";
  $username="";
 
} ?>

  
      <h3>Edit Profil</h3>
     
      <form action="<?php echo base_url('index.php/pelapor/editprofil')?>">
        <!-- <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" class="form-control" value="<?php echo $nama?>" id="nama" placeholder="Nama">
        </div>
       
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" class="form-control" value="<?php echo $username?>" id="username" name="username" placeholder="Email">
        </div> -->
       
        <div class="form-group">
          <label for="password">Kata Sandi Lama</label>
          <input type="password" class="form-control" id="password" name="password" placeholder="Kata Sandi">
        </div>
        <div class="form-group">
          <label for="newPassword">Kata Sandi Baru</label>
          <input type="password" class="form-control" id="newpassword" name="newpassword" placeholder="Kata Sandi Baru">
        </div>
       
        <center>
          <button id="simpan" class="btn btn-primary">Simpan</button>
        </center>
      </form>
    </div>

    <footer class="fixed-bottom">&copy 2018 lacak.co.id All Rights Reserved</footer>




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
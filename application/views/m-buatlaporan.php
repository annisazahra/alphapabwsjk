<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Buat Laporan</title>



    <!-- Bootstrap CSS -->
    <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/m-buatlaporan.css">  
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <h6>Pelapor</h6>

      <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item">
            <a class="navbar-brand" id="lacak" href="<?php echo base_url().'index.php/pelapor/get_laporanku';?>">LACAK</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/pelapor/get_laporanku';?>">Laporanku<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/pelapor/mbuatlaporan';?>">Buat Laporan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="">Pengaturan Akun</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="">Tentang</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'index.php/alphacrud/logout';?>">Keluar</a>
          </li>
        </ul>
      </div>
    </nav>

    <div class="container">
      <ul class="nav nav-pills nav-fill">
        <li class="nav-item">
          <a class="nav-link" id="laporan" href="<?php echo base_url().'index.php/pelapor/get_laporanku';?>">Laporanku</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" id="buat" href="<?php echo base_url().'index.php/pelapor/mbuatlaporan';?>">Buat Laporan</a>
        </li>
      </ul>
  <form method="POST" action="<?php echo base_url()."index.php/pelapor/insert"; ?>" enctype="multipart/form-data">
        <h1>Form Laporan</h1>
        <div class="form-group">
          <label for="nama_infrastruktur">Nama Infrastruktur</label>
          <sup>*</sup>
          <input type="text" class="form-control" id="nama_infrastruktur" name="nama_infrastruktur"placeholder="Nama Infrastruktur">
        </div>
        <div class="form-group">
          <label for="bencana_penyebab">Bencana Penyebab</label>
          <sup>*</sup>
          <input type="text" class="form-control" id="bencana_penyebab" name="bencana_penyebab" placeholder="Bencana Penyebab">
        </div>
        <div class="form-group">
          <label for="desc_kerusakan">Deskripsi Kerusakan</label>
          <sup>*</sup>
          <textarea class="form-control" name="desc_kerusakan" id="desc_kerusakan" rows="3"></textarea>
        </div>
        <div class="form-group">
          <label for="jenis_infra">Jenis  Infrastruktur</label>
          <sup>*</sup>
          <select class="form-control" id="jenis_infra" name="jenis_infra">
       <option value="" selected>Pilih Jenis Infrastruktur</option>
       <option value="1">Puskesmas</option>
       <option value="2">Jembatan</option>
       <option value="3">Rumah Sakit</option>
       <option value="4">Jalan Raya</option>
       <option value="5">Jalan Tol</option>
       <option value="6">Jalur Rel Kereta Api</option>
       <option value="7">Lembaga Pemasyarakatan</option>
       <option value="8">Perkantoran</option>
       <option value="9">Jalan</option>
       <option value="10">Terowongan</option>
       <option value="11">Lampu Jalan</option>
       <option value="12">Rambu Lalu Lintas</option>
       <option value="13">Pelabuhan</option>
       <option value="14">Bandar Udara</option>
       <option value="15">Taman</option>
       <option value="16">Sekolah Negeri</option>
       <option value="17">Universitas Negeri</option>
       <option value="18">Stadion</option>
          </select>
        </div>
        <div class="form-group">
          <label for="tingkat_kerusakan">Tingkat Kerusakan</label>
          <sup>*</sup>
          <select class="form-control" id="tingkat_kerusakan" name="tingkat_kerusakan">
             <option value="" selected>Pilih Tingkat Kerusakan</option>
             <option value="1">Ringan</option>
             <option value="2">Sedang</option>
             <option value="3">Parah</option>
          </select>
        </div>
        <div class="form-group">
          <label for="alamat_infra">Alamat</label>
          <sup>*</sup>
          <textarea class="form-control" id="alamat_infra" name="alamat_infra" rows="3"></textarea>
        </div>
        <div class="form-group">
          <label for="provinsi">Provinsi</label>
          <sup>*</sup>
          <select class="form-control" id="provinsi" name="provinsi">
            <option value="">Pilih Provinsi</option>
        <?php
        foreach($provinsi as $row)
        {
        echo '<option value="'.$row->provinsi_id.'">'.$row->provinsi_name.'</option>';
        }
        ?>
          </select>
        </div>
        <div class="form-group">
          <label for="kab_kota">Kabupaten/Kota</label>
          <sup>*</sup>
          <select class="form-control" id="kab_kota" name="kab_kota">
            <option value="" >Pilih Kabupaten/Kota</option>
          </select>
        </div>
        <div class="form-group">
          <label for="kecamatan">Kecamatan</label>
          <sup>*</sup>
          <select class="form-control" id="kecamatan" name="kecamatan">
            <option value="" >Pilih Kecamatan</option>
          </select>
        </div>
        <div class="form-group">
          <label for="desa_kel">Desa/Kelurahan</label>
          <sup>*</sup>
          <select class="form-control" id="desa_kel" name="desa_kel">
            <option value="" >Pilih Desa/Kelurahan</option>
          </select>
        </div>
        <div class="form-group">
          <label for="pics">Unggah Foto</label>
          <sup>*</sup>
          <input type="file" class="form-control-file" name="pics" id="pics">
        </div>
        <center>
           <?php echo $this->session->userdata('id'); ?>
          <button id="kirim" class="btn btn-primary" onclick="alertKirim();">Kirim Laporan</button>
        </center>
      </form>
    </div>
    
    <footer class="fixed-bottom">&copy 2018 lacak.co.id All Rights Reserved</footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <!--  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="<?php echo base_url();?>/js/alert.js"></script>
  </body>
</html>
<script >
$(document).ready(function(){
 $('#provinsi').change(function(){
  var provinsi_id = $('#provinsi').val();
  if(provinsi_id != '')
  {
   $.ajax({
    url:"<?php echo base_url(); ?>index.php/alphacrud/fetch_kab_kota",
    method:"POST",
    data:{provinsi_id:provinsi_id},
    success:function(data)
    {
     $('#kab_kota').html(data);
     $('#kecamatan').html('<option value="">Pilih Kecamatan</option>');
     $('#desa_kel').html('<option value="">Pilih Desa/Kelurahan</option>');
    }
   });
  }
  else
  {
   $('#kab_kota').html('<option value="">Pilih Kabupaten/Kota</option>');
   $('#kecamatan').html('<option value="">Pilih Kecamatan</option>');
   $('#desa_kel').html('<option value="">Pilih Desa/Kelurahan</option>');

  }
 });

 $('#kab_kota').change(function(){
  var kab_kota_id = $('#kab_kota').val();
  if(kab_kota_id != '')
  {
   $.ajax({
    url:"<?php echo base_url(); ?>index.php/alphacrud/fetch_kecamatan",
    method:"POST",
    data:{kab_kota_id:kab_kota_id},
    success:function(data)
    {
     $('#kecamatan').html(data);
     $('#desa_kel').html('<option value="">Pilih Desa/Kelurahan</option>');

    }
   });
  }
  else
  {
   $('#kecamatan').html('<option value="">Pilih Kecamatan</option>');
   $('#desa_kel').html('<option value="">Pilih Desa/Kelurahan</option>');

  }
 });

 $('#kecamatan').change(function(){
  var kecamatan_id = $('#kecamatan').val();
  if(kecamatan_id != '')
  {
   $.ajax({
    url:"<?php echo base_url(); ?>index.php/alphacrud/fetch_desa_kel",
    method:"POST",
    data:{kecamatan_id:kecamatan_id},
    success:function(data)
    {
     $('#desa_kel').html(data);
    }
   });
  }
  else
  {
   $('#desa_kel').html('<option value="">Pilih Desa/Kelurahan</option>');
  }
 });
 
});
</script>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dinas extends CI_Controller {

	function __construct(){

	parent::__construct();

	$this->load->helper('url');
	$this->load->helper('form');
	$this->load->model('alphamodel');
	$this->load->library('session');

	if($this->session->userdata('level')!="Dinas"){
		redirect('alphacrud/indexxx');
	}

}
function dlihatlaporan($nomor_laporan){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->data['dataEdit']=$this->alphamodel->dataEdit('formlaporan',$nomor_laporan);
		$this->load->view('d-lihatlaporan',$this->data);

	}

public function update($nomor_laporan){
		$this->load->library('session');
		
		$username= $this->session->userdata('username');
		$nama_infrastruktur = $_POST['nama_infrastruktur'];
		$bencana_penyebab = $_POST['bencana_penyebab'];
		$desc_kerusakan = $_POST['desc_kerusakan'];
		$idJenis = $_POST['jenis_infra'];
		$idTingkat = $_POST['tingkat_kerusakan'];
		$alamat_infra = $_POST['alamat_infra'];
		$provinsi = $_POST['provinsi'];
		$kab_kota = $_POST['kab_kota'];
		$kecamatan = $_POST['kecamatan'];
		$desa_kel = $_POST['desa_kel'];
		$status = $_POST['status'];
		$dinas = $_POST['dinas'];


		$data = array('nama_infrastruktur'=>$nama_infrastruktur, 'bencana_penyebab'=>$bencana_penyebab, 'desc_kerusakan'=>$desc_kerusakan,'jenis_infra'=>$idJenis, 'tingkat_kerusakan'=>$idTingkat, 'alamat_infra'=>$alamat_infra,'provinsi'=>$provinsi,'kab_kota' => $kab_kota,'kecamatan'=>$kecamatan, 'desa_kel' => $desa_kel, 'status'=>$status,'dinas'=>$dinas);
		$edit = $this->alphamodel->editData('formlaporan',$data,$nomor_laporan);
		if($edit>0){
			redirect('dinas/dceklaporan');
		}else{
			echo 'Gagal disimpan';
		}

}

public function indexchart()
    {
    	$data['title'] = "chart";
    	$this->load->view('d-statistik', $data);
    }

	function index(){
		redirect('dinas/get_laporanku');
	}
function urutkan(){
	$username= $this->session->userdata('username');
		$data['urutkan'] = "";
			$this->load->model('alphamodel');
			$data['urutkan'] = $this->alphamodel->urutkandinas($username);
			$this->load->view('d-urutkan', $data);
}

public function cari() 
	{
		$this->load->view('d-ceklaporan');
	}
	public function hasil()
	{
		$username= $this->session->userdata('username');
		$data2['cari'] = $this->alphamodel->cariOrang($username);
		$this->load->view('d-result', $data2);
	}
	function dceklaporan(){
		$username= $this->session->userdata('username');

		$data['data'] = "";
			$this->load->model('alphamodel');
			$data['data'] = $this->alphamodel->get_dinas($username);
			$this->load->view('d-ceklaporan', $data);
	}

	function get_laporanku(){
		$username= $this->session->userdata('username');

		$data['data'] = "";
			$this->load->model('alphamodel');
			$data['data'] = $this->alphamodel->get_dinas($username);
			$this->load->view('d-ceklaporan', $data);

	}

}
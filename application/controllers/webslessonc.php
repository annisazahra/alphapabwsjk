 <?php  
 defined('BASEPATH') OR exit('No direct script access allowed');  
 class Webslessonc extends CI_Controller {  
      //functions  
      function index(){  
           $data["title"] = "Codeigniter Ajax CRUD with Data Tables and Bootstrap Modals";  
           $this->load->view('webslessonv', $data);  
      }  
      function fetch_user(){  
           $this->load->model("webslessonm");  
           $fetch_data = $this->webslessonm->make_datatables();  
           $data = array();  
           foreach($fetch_data as $row)  
           {  
                $sub_array = array();  
                $sub_array[] = $row->nomor_laporan;
                $sub_array[] = $row->nama_infrastruktur;  
                $sub_array[] = $row->bencana_penyebab;
                $sub_array[] = $row->desc_kerusakan;
                $sub_array[] = $row->jenis_infra;
                $sub_array[] = $row->tingkat_kerusakan;
                $sub_array[] = $row->alamat_infra;
                $sub_array[] = $row->provinsi;
                $sub_array[] = $row->kab_kota;
                $sub_array[] = $row->kecamatan;
                $sub_array[] = $row->desa_kel;
                $sub_array[] = $row->status;
                $sub_array[] = '<button type="button" name="update" id="'.$row->nomor_laporan.'" class="btn btn-warning btn-xs update">Update</button>';  
                $sub_array[] = '<button type="button" name="delete" id="'.$row->nomor_laporan.'" class="btn btn-danger btn-xs delete">Delete</button>';  
                $data[] = $sub_array;  
           }  
           $output = array(  
                "draw"                    =>     intval($_POST["draw"]),  
                "recordsTotal"          =>      $this->webslessonm->get_all_data(),  
                "recordsFiltered"     =>     $this->webslessonm->get_filtered_data(),  
                "data"                    =>     $data  
           );  
           echo json_encode($output);  
      }  

       function user_action(){  
           if($_POST["action"] == "Add")  
           {  
                $insert_data = array(  
                     'nama_infrastruktur' => $this->input->post('nama_infrastruktur'),  
                     'bencana_penyebab' => $this->input->post("bencana_penyebab"),  
                     'desc_kerusakan' => $this->input->post('desc_kerusakan'),  
                     'jenis_infra' => $this->input->post('jenis_infra'),  
                     'tingkat_kerusakan' => $this->input->post('tingkat_kerusakan'),  
                     'alamat_infra' => $this->input->post('alamat_infra'),  
                     'provinsi' => $this->input->post('provinsi'),  
                     'kab_kota' => $this->input->post('kab_kota'),  
                     'kecamatan' => $this->input->post('kecamatan'),  
                     'desa_kel' => $this->input->post('desa_kel'),  
                     'status' => $this->input->post('status'),  

                  
                );  
                $this->load->model('webslessonm');  
                $this->webslessonm->insert_crud($insert_data);  
                echo 'Data Inserted';  
           }  
           if($_POST["action"] == "Edit")  
           {  
                
                $updated_data = array(  
                     'nama_infrastruktur' => $this->input->post('nama_infrastruktur'),  
                     'bencana_penyebab' => $this->input->post('bencana_penyebab'), 
                     'desc_kerusakan' => $this->input->post('desc_kerusakan'),  
                     'jenis_infra' => $this->input->post('jenis_infra'),  
                     'tingkat_kerusakan' => $this->input->post('tingkat_kerusakan'),  
                     'alamat_infra' => $this->input->post('alamat_infra'),  
                     'provinsi' => $this->input->post('provinsi'),  
                     'kab_kota' => $this->input->post('kab_kota'),  
                     'kecamatan' => $this->input->post('kecamatan'),  
                     'desa_kel' => $this->input->post('desa_kel'),  
                     'status' => $this->input->post('status'), 
                );  
                $this->load->model('webslessonm');  
                $this->webslessonm->update_crud($this->input->post("nomor_laporan"), $updated_data);  
                echo 'Data Updated';  
           }  
      }  

       function fetch_single_user()  
      {  
           $output = array();  
           $this->load->model("webslessonm");  
           $data = $this->webslessonm->fetch_single_user($_POST["nomor_laporan"]);  
           foreach($data as $row)  
           {  
                $output['nama_infrastruktur'] = $row->nama_infrastruktur;  
                $output['bencana_penyebab'] = $row->bencana_penyebab;  
                $output['nomor_laporan'] = $row->nomor_laporan;
                $output['desc_kerusakan'] = $row->desc_kerusakan;  
                $output['jenis_infra'] = $row->jenis_infra;  
                $output['tingkat_kerusakan'] = $row->tingkat_kerusakan;  
                $output['alamat_infra'] = $row->alamat_infra;  
                $output['provinsi'] = $row->provinsi; 
                $output['kab_kota'] = $row->kab_kota;  
                $output['kecamatan'] = $row->kecamatan;  
                $output['des_kel'] = $row->des_kel;  
                $output['status'] = $row->status;  
               
           }  
           echo json_encode($output);  
      }  
 } 
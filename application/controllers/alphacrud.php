<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class Alphacrud extends CI_Controller {



	function __construct()
	{
	parent::__construct();

	$this->load->helper('url');
	$this->load->helper('form');
	$this->load->model('alphamodel');
	$this->load->library('session');
	}
    
    public function indexregister()
    {
    	$this->load->view("daftar");
    }

    public function register()
    {
    	$level = "Pelapor";
    	$user=array(
    		'nama'=>$this->input->post('nama'),
    		'username'=>$this->input->post('username'),
    		'password'=>md5($this->input->post('password')),
     		'level'=>$level
    	);

    	if (!$user['username']==''&&!$user['username']==''){
    		$this->alphamodel->register($user);
    		$this->session->set_flashdata('smsg','Registration Success');
    		redirect('alphacrud/indexregister');
    	}
    	else{
    		$this->session->set_flashdata('emsg','nama dan password tidak boleh kosong');
    		redirect('alphacrud/indexregister');
    	}
    }

    public function indexchart()
    {
    	$data['title'] = "chart";
    	$this->load->view('v_chart', $data);
    }
    public function getData(){
    
    	 $this->load->model('alphamodel');
    	 $data = $this->alphamodel->m_datachart();
    	 echo json_encode($data);

    	
    }


	function ambildata(){
		$dataLaporan=$this->alphamodel->ambildata('formlaporan')->result();
		echo json_encode($dataLaporan);
	}


	function laporanku(){
		$dataaaa['dataaaa'] = "";
			$this->load->model('alphamodel');
			$dataaaa['dataaaa'] = $this->alphamodel->get_laporanku();
			$this->load->view('teslaporan', $dataaaa);
	}



	function mbuatlaporan(){

		$data['provinsi'] = $this->alphamodel->fetch_provinsi();
		$this->load->view('m-buatlaporan',$data);
	}

	 function fetch_kab_kota()
 	{
  		if($this->input->post('provinsi_id'))
  		{
  		 echo $this->alphamodel->fetch_kab_kota($this->input->post('provinsi_id'));
 		}
	}

	function fetch_kecamatan()
	{
  		if($this->input->post('kab_kota_id'))
 		{
   		echo $this->alphamodel->fetch_kecamatan($this->input->post('kab_kota_id'));
 		}
	}

	function fetch_desa_kel()
	{
  		if($this->input->post('kecamatan_id'))
 		{
   		echo $this->alphamodel->fetch_desa_kel($this->input->post('kecamatan_id'));
 		}
	}

	function indexxx(){
		$this->load->view('beranda.php');
	}

function mmasuk(){
		$this->load->view('m-masuk');
	}
	function bmasuk(){
		$this->load->view('b-masuk');
	}
	function dmasuk(){
		$this->load->view('d-masuk');
	}
function kontak(){
		$this->load->view('kontak');
	}
	function daftar(){
		$this->load->view('daftar');
	}

	function home(){
		$this->load->view('home');
	}
	
	function login_m(){
		$this->load->view('login_m');
	}
	function login_b(){
		$this->load->view('login_b');
	}
	function login_d(){
		$this->load->view('login_d');
	}
	function cek_laporan(){
		$this->load->view('cek_laporan');
	}
	
function masuk(){
		$this->load->view('masuk');
	}

	function proses_login(){
		$user=$this->input->post('username');
		$pass=$this->input->post('password');
		$passwordx = md5($pass);

		$ceklogin=$this->alphamodel->login($user,$passwordx);

		if($ceklogin){
			foreach($ceklogin as $row);
			$this->session->set_userdata('username',$row->username);
			$this->session->set_userdata('level',$row->level);
			

			if($this->session->userdata('level')=="Dinas"){
				redirect('alphacrud/mmasuk');
			}elseif($this->session->userdata('level')=="Pelapor"){
				redirect('pelapor/index');
			}elseif($this->session->userdata('level')=="BPBD"){
				redirect('alphacrud/mmasuk');
			}
		}else{
			$dataaaaaa['pesan']="Email atau Password tidak sesuai.";
			$this->load->view('m-masuk',$dataaaaaa);
		}

	}
		function proses_loginbpbd(){
		$user=$this->input->post('username');
		$pass=$this->input->post('password');

		$ceklogin=$this->alphamodel->login($user,$pass);

		if($ceklogin){
			foreach($ceklogin as $row);
			$this->session->set_userdata('username',$row->username);
			$this->session->set_userdata('level',$row->level);
			

			if($this->session->userdata('level')=="Dinas"){
				redirect('alphacrud/bmasuk');
			}elseif($this->session->userdata('level')=="Pelapor"){
				redirect('alphacrud/bmasuk');
			}elseif($this->session->userdata('level')=="BPBD"){
				redirect('bpbd/index');
			}
		}else{
			$dataaaaaa['pesan']="Username atau Password tidak sesuai.";
			$this->load->view('b-masuk',$dataaaaaa);
		}

	}

function proses_logindinas(){
		$user=$this->input->post('username');
		$pass=$this->input->post('password');

		$ceklogin=$this->alphamodel->login($user,$pass);

		if($ceklogin){
			foreach($ceklogin as $row);
			$this->session->set_userdata('username',$row->username);
			$this->session->set_userdata('level',$row->level);
			

			if($this->session->userdata('level')=="Dinas"){
				redirect('dinas/index');
			}elseif($this->session->userdata('level')=="Pelapor"){
				redirect('alphacrud/dmasuk');
			}elseif($this->session->userdata('level')=="BPBD"){
				redirect('alphacrud/dmasuk');
			}
		}else{
			$dataaaaaa['pesan']="Username atau Password tidak sesuai.";
			$this->load->view('d-masuk',$dataaaaaa);
		}

	}
	public function logout(){
		$this->session->sess_destroy();
		redirect('alphacrud/indexxx');
	}

	public function index(){
	
		$data['data'] = "";
			$this->load->model('alphamodel');
			$data['data'] = $this->alphamodel->get_formlaporan();
			$this->load->view('alphaviewlaporan', $data);
	}

	public function indexx(){
	
		$dataa['dataa'] = "";
			$this->load->model('alphamodel');
			$dataa['dataa'] = $this->alphamodel->get_formlaporan();
			$this->load->view('detaillaporan', $dataa);
	}


		public function newform_add(){
		$this->load->view('alphaformlaporan');
	}
	
	
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelapor extends CI_Controller {

	function __construct(){

	parent::__construct();

	$this->load->helper('url');
	$this->load->helper('form');
	$this->load->model('alphamodel');
	$this->load->library('session');

	if($this->session->userdata('level')!="Pelapor"){
		redirect('alphacrud/indexxx');
	}

}

public function insert(){
		$this->load->library('session');

		$username= $this->session->userdata('username');

		$status="1";
		$dinas="";

		$this->load->helper('string');
		$gambar = random_string('alnum',10);

		
		$file_tmp = $_FILES['pics']['tmp_name'];
		$file_type = $_FILES['pics']['type'];
		$file_error= $_FILES['pics']['error'];
		$file_size = $_FILES['pics']['size'];

		if($file_type == "image/jpeg"){
			$file_path = "assets/images/" . $gambar . ".jpg";
			}
		elseif ($file_type == "image/png") {
			$file_path = "assets/images/" . $gambar . ".png";
			}

		$input = array(
			'nama_infrastruktur'=>$_POST['nama_infrastruktur'], 
			'bencana_penyebab'=>$_POST['bencana_penyebab'], 
			'desc_kerusakan'=>$_POST['desc_kerusakan'],
			'jenis_infra'=>$_POST['jenis_infra'], 
			'tingkat_kerusakan'=>$_POST['tingkat_kerusakan'], 
			'alamat_infra'=>$_POST['alamat_infra'],
			'provinsi'=>$_POST['provinsi'],
			'kab_kota' => $_POST['kab_kota'],
			'kecamatan'=>$_POST['kecamatan'], 
			'desa_kel' => $_POST['desa_kel'], 
			'username'=>$username, 
			'status'=>$status,
			'dinas'=>$dinas,
			'url'=>$file_path,
			'created_date' => date('Y-m-d'));

		if($file_type == "image/jpeg"){
			if($file_size>0 AND $file_error==0){
				move_uploaded_file($file_tmp, "assets/images/" . $gambar . ".jpg");
			}
		}elseif ($file_type == "image/png") {
			if($file_size>0 AND $file_error==0){
				move_uploaded_file($file_tmp, "assets/images/" . $gambar . ".png");
			}
		}

		$this->alphamodel->add_laporan($input,$file_path);
		redirect('pelapor/mbuatlaporan');

	}

function buatlaporan(){

		$data['provinsi'] = $this->alphamodel->fetch_provinsi();
		$this->load->view('buat_laporan',$data);
	}

function edit($nomor_laporan){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->data['dataEdit']=$this->alphamodel->dataEdit('formlaporan',$nomor_laporan);
		$this->load->view('m-lihatlaporan',$this->data);

	}

	function get_laporanku(){
		$username= $this->session->userdata('username');

		$dataaaa['dataaaa'] = "";
			$this->load->model('alphamodel');
			$dataaaa['dataaaa'] = $this->alphamodel->get_laporanku($username);
			$this->load->view('m-laporanku', $dataaaa);

	}
	function index(){
			redirect('pelapor/get_laporanku');
	}


	public function cari() 
	{
		$this->load->view('m-laporanku');
	}
	public function hasil()
	{
		$username= $this->session->userdata('username');
		$data2['cari'] = $this->alphamodel->cariOrangPelapor($username);
		$this->load->view('m-result', $data2);
	}
function urutkan(){
	$username= $this->session->userdata('username');
	$data['urutkan'] = $this->alphamodel->urutkan($username);
		$this->load->view('m-urutkan', $data);
}
function mberanda(){
	$this->load->view('m-beranda');
}
function mlihatlaporan(){
	$this->load->view('m-lihatlaporan');
}
function mbuatlaporan(){
	$data['provinsi'] = $this->alphamodel->fetch_provinsi();
		$this->load->view('m-buatlaporan',$data);
}
function mlaporanku(){
	$this->load->view('m-laporanku');
}
function meditprofil(){
	$this->load->view('m-editprofil');
}

	function loadview(){
		$this->load->view('alphaviewlaporan');
	}

	function fetch()
 {

 $this->load->library('session');

$username= $this->session->userdata('username');
  $output = '';
  $query = '';
  $this->load->model('alphamodel');
  if($this->input->post('query'))
  {
   $query = $this->input->post('query');
  }
  $data = $this->alphamodel->fetch_data($query,$username);
  if($data->num_rows() > 0)
  {
   foreach($data->result() as $row)
   {
    $output .= '
      <tr>
       <td>'.$row->nama_infrastruktur.'</td>
       <td>'.$row->nomor_laporan.'</td>
      </tr>
    ';
   }
  }
  else
  {
   $output .= '<tr>
       <td colspan="5">No Data Found</td>
      </tr>';
  }
  $output .= '</table>';
  echo $output;
 }
 function editprofil(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$usernamee= $this->session->userdata('username');
		$dataa = $this->alphamodel->get_iduser($usernamee);
		$iduser = $dataa;

		$this->data['dataEditProfil']=$this->alphamodel->dataEditProfil('user',$iduser);
		$this->load->view('m-editprofil',$this->data);

	}

 function updateprofil($iduser){
 	$this->load->library('session');
		
		$usernamee= $this->session->userdata('username');
		// $passwordlama= $this->session->userdata('password');
		// $passwordx = md5($passwordlama);
		$nama = $_POST['nama'];
		$username = $_POST['username'];
		// $password = $_POST['password'];
		// $newpassword = $_POST['newpassword'];

	// if($password == $passwordx){
		$data = array('nama'=>$nama, 'username'=>$username);
		$edit = $this->alphamodel->editprofil('user',$data,$iduser);
		if($edit>0){
			redirect('pelapor/index');
		}else{
			echo 'Gagal disimpan';
		}
	// }else{
	// 	echo 'kata sandi lama tidak sesuai';
	// }
 }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bpbd extends CI_Controller {

	function __construct(){

	parent::__construct();

	$this->load->helper('url');
	$this->load->helper('form');
	$this->load->model('alphamodel');
	$this->load->library('session');

	if($this->session->userdata('level')!="BPBD"){
		redirect('alphacrud/indexxx');
	}

	}
	function get_bpbd(){
		$this->load->library('session');
		$username= $this->session->userdata('username');
		$dataa = $this->alphamodel->get_provinsi($username);
		$provinsi = $dataa;

		$data['data'] = "";
			$this->load->model('alphamodel');
			$data['data'] = $this->alphamodel->get_formlaporanbpbd($provinsi);
			$this->load->view('b-beranda', $data);
	}
function urutkan(){
	$username= $this->session->userdata('username');
	$dataa = $this->alphamodel->get_provinsi($username);
		$kab_kota = $dataa;

		$data['urutkan'] = "";
			$this->load->model('alphamodel');
			$data['urutkan'] = $this->alphamodel->urutkanbpbd($kab_kota);
			$this->load->view('b-urutkan', $data);
}
public function cari() 
	{
		$this->load->view('b-beranda');
	}
	public function hasil()
	{
		$username= $this->session->userdata('username');
		$dataa = $this->alphamodel->get_provinsi($username);

		$data2['cari'] = $this->alphamodel->cariOrangBpbd($dataa);
		$this->load->view('b-result', $data2);
	}
function editbpbd($nomor_laporan){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->data['dataEdit']=$this->alphamodel->dataEdit('formlaporan',$nomor_laporan);
		$this->load->view('b-lihatlaporan',$this->data);

	}

	function index(){
		redirect('bpbd/get_bpbd');
	}

	public function update($nomor_laporan){
		$this->load->library('session');
		
		$username= $this->session->userdata('username');
		$nama_infrastruktur = $_POST['nama_infrastruktur'];
		$bencana_penyebab = $_POST['bencana_penyebab'];
		$desc_kerusakan = $_POST['desc_kerusakan'];
		$idJenis = $_POST['jenis_infra'];
		$idTingkat = $_POST['tingkat_kerusakan'];
		$alamat_infra = $_POST['alamat_infra'];
		$provinsi = $_POST['provinsi'];
		$kab_kota = $_POST['kab_kota'];
		$kecamatan = $_POST['kecamatan'];
		$desa_kel = $_POST['desa_kel'];
		$status = $_POST['status'];
		$dinas = $_POST['dinas'];


		$data = array('nama_infrastruktur'=>$nama_infrastruktur, 'bencana_penyebab'=>$bencana_penyebab, 'desc_kerusakan'=>$desc_kerusakan,'jenis_infra'=>$idJenis, 'tingkat_kerusakan'=>$idTingkat, 'alamat_infra'=>$alamat_infra,'provinsi'=>$provinsi,'kab_kota' => $kab_kota,'kecamatan'=>$kecamatan, 'desa_kel' => $desa_kel, 'status'=>$status,'dinas'=>$dinas);
		$edit = $this->alphamodel->editData('formlaporan',$data,$nomor_laporan);
		if($edit>0){
			redirect('bpbd/get_bpbd');
		}else{
			echo 'Gagal disimpan';
		}

}

function ceklaporan(){
		$this->load->view('b-ceklaporan');
	}
	function beranda(){
		$this->load->view('b-beranda');
	}
	function kirimkedinas(){
		$this->load->view('b-kirimkedinas');
	}
	
	function chat(){
		$this->load->view('b-chat');
	}
	function tentang(){
		$this->load->view('b-tentang');
	}
function lihatlaporan(){
		$this->load->view('b-lihatlaporan');
	}
	public function indexchart()
    {
    	$data['title'] = "chart";
    	$this->load->view('b-statistik', $data);
    }
}
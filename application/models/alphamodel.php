<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Alphamodel extends CI_Model {

	function register($user){
		$this->db->insert('user',$user);
	}

function fetch_data($query,$username)
 {
  $this->db->select("*");
  $this->db->from("formlaporan");
  $this->db->where('username',$username);
  if($query != '')
  {
   $this->db->like('nama_infrastruktur', $query);
   $this->db->or_like('nomor_laporan', $query);
  $this->db->order_by('nomor_laporan', 'ASC');
  return $this->db->get();
 }
}

	 function fetch_provinsi()
 {
  $this->db->order_by("provinsi_name", "ASC");
  $query = $this->db->get("provinsi");
  return $query->result();
 }

 function fetch_kab_kota($provinsi_id)
 {
  $this->db->where('provinsi_id', $provinsi_id);
  $this->db->order_by('kab_kota_name', 'ASC');
  $query = $this->db->get('kab_kota');
  $output = '<option value="">Pilih Kabupaten/Kota</option>';
  foreach($query->result() as $row)
  {
   $output .= '<option value="'.$row->kab_kota_id.'">'.$row->kab_kota_name.'</option>';
  }
  return $output;
 }

 function fetch_kecamatan($kab_kota_id)
 {
  $this->db->where('kab_kota_id', $kab_kota_id);
  $this->db->order_by('kecamatan_name', 'ASC');
  $query = $this->db->get('kecamatan');
  $output = '<option value="">Pilih Kecamatan</option>';
  foreach($query->result() as $row)
  {
   $output .= '<option value="'.$row->kecamatan_id.'">'.$row->kecamatan_name.'</option>';
  }
  return $output;
 }

 function fetch_desa_kel($kecamatan_id)
 {
  $this->db->where('kecamatan_id', $kecamatan_id);
  $this->db->order_by('desa_kel_name', 'ASC');
  $query = $this->db->get('desa_kel');
  $output = '<option value="">Pilih Desa/Kelurahan</option>';
  foreach($query->result() as $row)
  {
   $output .= '<option value="'.$row->desa_kel_id.'">'.$row->desa_kel_name.'</option>';
  }
  return $output;
 }

	public function m_datachart()
	{
		$query = "SELECT b.namaJenis as jenisInfra, count(a.jenis_infra) as jumlah from formlaporan as a left join jenisinfrastruktur as b ON b.idJenis=a.jenis_infra GROUP BY a.jenis_infra order by b.namaJenis asc";
		return $this->db->query($query)->result();
	}
	public function cariOrang($username)
	{
		$cari = $this->input->GET('cari', TRUE);
		$data = $this->db->query("SELECT * from formlaporan as a left join status b ON b.id=a.status where (a.nomor_laporan like '%$cari%' OR a.nama_infrastruktur like '%$cari%' OR b.nama like '%$cari%')  AND a.dinas='$username' GROUP BY a.status");
		return $data->result();
	}

public function cariOrangBpbd($kab_kota)
	{
		$cari = $this->input->GET('cari', TRUE);
		$data = $this->db->query("SELECT * from formlaporan as a left join status b ON b.id=a.status left join kab_kota k ON k.kab_kota_id =a.kab_kota where (a.nomor_laporan like '%$cari%' OR a.nama_infrastruktur like '%$cari%' OR b.nama like '%$cari%')  AND k.kab_kota_name='$kab_kota' GROUP BY a.status");
		return $data->result();
	}
public function cariOrangPelapor($username)
	{
		$cari = $this->input->GET('cari', TRUE);
		$data = $this->db->query("SELECT * from formlaporan as a left join status b ON b.id=a.status where (a.nomor_laporan like '%$cari%' OR a.nama_infrastruktur like '%$cari%' OR b.nama like '%$cari%')  AND a.username='$username' GROUP BY a.status");
		return $data->result();
	}
		public function urutkan($username)
	{
		$urutkan = $this->input->GET('urutkan', TRUE);
		$this->db->select(['nama_infrastruktur', 'nomor_laporan','bencana_penyebab','desc_kerusakan','alamat_infra','provinsi','kab_kota','kecamatan','desa_kel','jenis_infra','tingkat_kerusakan','status','created_date']);
		$this->db->from('formlaporan');
		$this->db->where('username',$username);
		if ($urutkan=="nomor_laporan"){
		$this->db->order_by('nomor_laporan','asc');
		}elseif ($urutkan=="created_date") {
		$this->db->order_by('created_date','asc');
		}elseif ($urutkan=="nama_infrastruktur") {
		$this->db->order_by('nama_infrastruktur','asc');
		}elseif ($urutkan=="status") {
		$this->db->order_by('status','asc');
		}

		$data=$this->db->get('');
		return $data->result();
	}
public function urutkandinas($username)
	{
		$urutkan = $this->input->GET('urutkan', TRUE);
		$this->db->select(['nama_infrastruktur', 'nomor_laporan','bencana_penyebab','desc_kerusakan','alamat_infra','provinsi','kab_kota','kecamatan','desa_kel','jenis_infra','tingkat_kerusakan','status','created_date']);
		$this->db->from('formlaporan');
		$this->db->where('dinas',$username);
		if ($urutkan=="nomor_laporan"){
		$this->db->order_by('nomor_laporan','asc');
		}elseif ($urutkan=="created_date") {
		$this->db->order_by('created_date','asc');
		}elseif ($urutkan=="nama_infrastruktur") {
		$this->db->order_by('nama_infrastruktur','asc');
		}elseif ($urutkan=="status") {
		$this->db->order_by('status','asc');
		}

		$data=$this->db->get('');
		return $data->result();
	}
	public function urutkanbpbd($kab_kota)
	{
		$urutkan = $this->input->GET('urutkan', TRUE);
			$this->db->select(['f.nomor_laporan','f.nama_infrastruktur','f.bencana_penyebab','f.desc_kerusakan','f.alamat_infra','f.provinsi','f.kab_kota','f.kecamatan','f.desa_kel','f.jenis_infra','f.tingkat_kerusakan','f.status','f.created_date','p.provinsi_id','p.provinsi_name','k.kab_kota_id','k.kab_kota_name','c.kecamatan_id','c.kecamatan_name','d.desa_kel_id','d.desa_kel_name','j.idJenis','j.namaJenis','t.idTingkatan','t.namaTingkatan','s.id','s.nama']);
			$this->db->from('formlaporan f');
			$this->db->join('provinsi p', 'p.provinsi_id = f.provinsi', 'left');
			$this->db->join('kab_kota k', 'k.kab_kota_id = f.kab_kota', 'left');
			$this->db->join('kecamatan c', 'c.kecamatan_id = f.kecamatan', 'left');
			$this->db->join('desa_kel d', 'd.desa_kel_id = f.desa_kel', 'left');
			$this->db->join('jenisinfrastruktur j', 'j.idJenis = f.jenis_infra', 'left');
			$this->db->join('tingkatkerusakan t','t.idTingkatan=f.tingkat_kerusakan', 'left');
			$this->db->join('status s','s.id=f.status', 'left');
			$this->db->where('kab_kota_name',$kab_kota);
			
		if ($urutkan=="nomor_laporan"){
		$this->db->order_by('nomor_laporan','asc');
		}elseif ($urutkan=="created_date") {
		$this->db->order_by('created_date','asc');
		}elseif ($urutkan=="nama_infrastruktur") {
		$this->db->order_by('nama_infrastruktur','asc');
		}elseif ($urutkan=="status") {
		$this->db->order_by('status','asc');
		}

		$data=$this->db->get('');
		return $data->result();
	}
 function get_data_barang_bykode($nomor_laporan){
        $hsl=$this->db->query("SELECT * FROM formlaporan WHERE nomor_laporan='$nomor_laporan'");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'nomor_laporan' => $data->nomor_laporan,
                    'nama_infrastruktur' => $data->nama_infrastruktur,
                    'bencana_penyebab' => $data->bencana_penyebab,
                   
                    );
            }
        }
        return $hasil;
    }

	function fetch_single_user($nomor_laporan){
		$this->db->where("nomor_laporan",$nomor_laporan);
		$query=$this->db->get('users');
		return $query->result();
	}


	function plusdata($data,$table){
		$this->db->insert($table,$data);
	}

	function dataEdit($table_name,$nomor_laporan){
		$this->db->where('nomor_laporan',$nomor_laporan);
		$get_laporan = $this->db->get($table_name);
		return $get_laporan->row();
	}
	function dataEditProfil($table_name,$iduser){
		$this->db->where('id',$iduser);
		$get_laporan = $this->db->get($table_name);
		return $get_laporan->row();
	}

	function ambildata($table){
		$this->db->select(['f.nomor_laporan','f.nama_infrastruktur','f.bencana_penyebab','f.desc_kerusakan','f.alamat_infra','f.provinsi','f.kab_kota','f.kecamatan','f.desa_kel','f.jenis_infra','f.tingkat_kerusakan','f.status','p.provinsi_id','p.provinsi_name','k.kab_kota_id','k.kab_kota_name','c.kecamatan_id','c.kecamatan_name','d.desa_kel_id','d.desa_kel_name','j.idJenis','j.namaJenis','t.idTingkatan','t.namaTingkatan','s.id','s.nama']);
			$this->db->from('formlaporan f');
			$this->db->join('provinsi p', 'p.provinsi_id = f.provinsi', 'left');
			$this->db->join('kab_kota k', 'k.kab_kota_id = f.kab_kota', 'left');
			$this->db->join('kecamatan c', 'c.kecamatan_id = f.kecamatan', 'left');
			$this->db->join('desa_kel d', 'd.desa_kel_id = f.desa_kel', 'left');
			$this->db->join('jenisinfrastruktur j', 'j.idJenis = f.jenis_infra', 'left');
			$this->db->join('tingkatkerusakan t','t.idTingkatan=f.tingkat_kerusakan', 'left');
			$this->db->join('status s','s.id=f.status', 'left');
			$this->db->order_by('nomor_laporan', 'asc');
			return $this->db->get($table);
		
	}


	function login($user,$pass){
		$this->db->select('username,password,level');
		$this->db->from('user');
		$this->db->where('username',$user);
		$this->db->where('password',$pass);
		$this->db->limit(1);

		$query = $this->db->get();

		if($query->num_rows()==1){
			return $query->result();
		}else{
			return false;
		}
	}

	public function add_laporan($input,$file_path){
		$query="INSERT INTO formlaporan VALUES(

		'".$input['nama_infrastruktur']."',
		'".$input['bencana_penyebab']."',
		'".$input['desc_kerusakan']."',
		'".$input['alamat_infra']."',
		'".$input['provinsi']."',
		'".$input['kab_kota']."',
		'".$input['kecamatan']."',
		'".$input['desa_kel']."',
		'".$input['nomor_laporan']."',
		'".$input['jenis_infra']."',
		'".$input['tingkat_kerusakan']."',
		'".$input['username']."',
		'".$input['status']."',
		'".$input['dinas']."',
		'".$file_path."',
		'".$input['created_date']."'
		
	)";
	$this->db->query($query);
	}

	public function get_laporanku($username){
		$this->db->select(['nama_infrastruktur', 'nomor_laporan','bencana_penyebab','desc_kerusakan','alamat_infra','provinsi','kab_kota','kecamatan','desa_kel','jenis_infra','tingkat_kerusakan','status']);
		$this->db->from('formlaporan');
		$this->db->where('username',$username);
	
		$query=$this->db->get('');
		return $query->result();
	}

public function get_dinas($username){
		$this->db->select(['nama_infrastruktur', 'nomor_laporan','bencana_penyebab','desc_kerusakan','alamat_infra','provinsi','kab_kota','kecamatan','desa_kel','jenis_infra','tingkat_kerusakan','status']);
		$this->db->from('formlaporan');
		$this->db->where('dinas',$username);
	
		$query=$this->db->get('');
		return $query->result();
	}

public function get_ajax($nomor_laporan){
		$this->db->select(['nama_infrastruktur', 'nomor_laporan','bencana_penyebab']);
		$this->db->from('formlaporan');
		$this->db->where('nomor_laporan',$nomor_laporan);
	
		$query=$this->db->get('');
		return $query->result();
	}

public function editData($table_name,$data,$nomor_laporan){
		$this ->db->where('nomor_laporan',$nomor_laporan);
		$edit = $this->db->update($table_name,$data);
		return $edit;
	}
public function editprofil($table_name,$data,$iduser){
		$this ->db->where('id',$iduser);
		$edit = $this->db->update($table_name,$data);
		return $edit;
	}

	public function get_formlaporan()
		{
			$this->db->select(['f.nomor_laporan','f.nama_infrastruktur','f.bencana_penyebab','f.desc_kerusakan','f.alamat_infra','f.provinsi','f.kab_kota','f.kecamatan','f.desa_kel','f.jenis_infra','f.tingkat_kerusakan','f.status','p.provinsi_id','p.provinsi_name','k.kab_kota_id','k.kab_kota_name','c.kecamatan_id','c.kecamatan_name','d.desa_kel_id','d.desa_kel_name','j.idJenis','j.namaJenis','t.idTingkatan','t.namaTingkatan','s.id','s.nama']);
			$this->db->from('formlaporan f');
			$this->db->join('provinsi p', 'p.provinsi_id = f.provinsi', 'left');
			$this->db->join('kab_kota k', 'k.kab_kota_id = f.kab_kota', 'left');
			$this->db->join('kecamatan c', 'c.kecamatan_id = f.kecamatan', 'left');
			$this->db->join('desa_kel d', 'd.desa_kel_id = f.desa_kel', 'left');
			$this->db->join('jenisinfrastruktur j', 'j.idJenis = f.jenis_infra', 'left');
			$this->db->join('tingkatkerusakan t','t.idTingkatan=f.tingkat_kerusakan', 'left');
			$this->db->join('status s','s.id=f.status', 'left');
			$this->db->order_by('nomor_laporan', 'asc');
			$return = $this->db->get('');
			return $return->result();
		}

			public function get_provinsi($username)
		{
			$this->db->where('username', $username);
			return $this->db->get('user')->row('kab_kota');

		}
			public function get_iduser($username)
		{
			$this->db->where('username', $username);
			return $this->db->get('user')->row('id');

		}


			public function get_formlaporanbpbd($provinsi)
		{
			$this->db->select(['f.nomor_laporan','f.nama_infrastruktur','f.bencana_penyebab','f.desc_kerusakan','f.alamat_infra','f.provinsi','f.kab_kota','f.kecamatan','f.desa_kel','f.jenis_infra','f.tingkat_kerusakan','f.status','p.provinsi_id','p.provinsi_name','k.kab_kota_id','k.kab_kota_name','c.kecamatan_id','c.kecamatan_name','d.desa_kel_id','d.desa_kel_name','j.idJenis','j.namaJenis','t.idTingkatan','t.namaTingkatan','s.id','s.nama']);
			$this->db->from('formlaporan f');
			$this->db->join('provinsi p', 'p.provinsi_id = f.provinsi', 'left');
			$this->db->join('kab_kota k', 'k.kab_kota_id = f.kab_kota', 'left');
			$this->db->join('kecamatan c', 'c.kecamatan_id = f.kecamatan', 'left');
			$this->db->join('desa_kel d', 'd.desa_kel_id = f.desa_kel', 'left');
			$this->db->join('jenisinfrastruktur j', 'j.idJenis = f.jenis_infra', 'left');
			$this->db->join('tingkatkerusakan t','t.idTingkatan=f.tingkat_kerusakan', 'left');
			$this->db->join('status s','s.id=f.status', 'left');
			$this->db->where('kab_kota_name',$provinsi);
			$this->db->order_by('nomor_laporan', 'asc');
			$return = $this->db->get('');
			return $return->result();
		}

		public function tambahData($foto,$data){
			$this->db->set('bukti',$foto);
		$qry=$this->db->insert('formlaporan',$data);
		if($qry){
			echo "File upload success";
		}else{
			echo "upload error";
		}
	
	}

	
	
}
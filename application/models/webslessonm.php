
 <?php  
 class Webslessonm extends CI_Model  
 {  
      var $table = "formlaporan";  
      var $select_column = array("nomor_laporan", "nama_infrastruktur", "bencana_penyebab","desc_kerusakan","alamat_infra","provinsi","kab_kota","kecamatan","desa_kel","jenis_infra","tingkat_kerusakan","status");  
      var $order_column = array("nomor_laporan", "nama_infrastruktur", "bencana_penyebab", "desc_kerusakan", "alamat_infra","provinsi","kab_kota","kecamatan","desa_kel","jenis_infra","tingkat_kerusakan","status",null);  
      function make_query()  
      {  
           $this->db->select($this->select_column);  
           $this->db->from($this->table);  
           if(isset($_POST["search"]["value"]))  
           {  
                $this->db->like("nama_infrastruktur", $_POST["search"]["value"]);  
                $this->db->or_like("bencana_penyebab", $_POST["search"]["value"]);  
                $this->db->or_like("nomor_laporan", $_POST["search"]["value"]);  
                $this->db->or_like("desc_kerusakan", $_POST["search"]["value"]);  
                $this->db->or_like("alamat_infra", $_POST["search"]["value"]);  
                $this->db->or_like("provinsi", $_POST["search"]["value"]);  
                $this->db->or_like("kab_kota", $_POST["search"]["value"]);  
                $this->db->or_like("kecamatan", $_POST["search"]["value"]);  
                $this->db->or_like("desa_kel", $_POST["search"]["value"]);  
                $this->db->or_like("jenis_infra", $_POST["search"]["value"]);  
                $this->db->or_like("tingkat_kerusakan", $_POST["search"]["value"]);  
                $this->db->or_like("status", $_POST["search"]["value"]);  

           }  
           if(isset($_POST["order"]))  
           {  
                $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
           }  
           else  
           {  
                $this->db->order_by('nomor_laporan', 'DESC');  
           }  
      }  
      function make_datatables(){  
           $this->make_query();  
           if($_POST["length"] != -1)  
           {  
                $this->db->limit($_POST['length'], $_POST['start']);  
           }  
           $query = $this->db->get();  
           return $query->result();  
      }  
      function get_filtered_data(){  
           $this->make_query();  
           $query = $this->db->get();  
           return $query->num_rows();  
      }       
      function get_all_data()  
      {  
           $this->db->select("*");  
           $this->db->from($this->table);  
           return $this->db->count_all_results();  
      }
       function insert_crud($data)  
      {  
           $this->db->insert('formlaporan', $data);  
      }  
      function fetch_single_user($nomor_laporan)  
      {  
           $this->db->where("nomor_laporan", $nomor_laporan);  
           $query=$this->db->get('formlaporan');  
           return $query->result();  
      }  
      function update_crud($nomor_laporan, $data)  
      {  
           $this->db->where("nomor_laporan", $nomor_laporan);  
           $this->db->update("formlaporan", $data);  
      }  
    }  
 